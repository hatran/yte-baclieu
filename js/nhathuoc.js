var datanhathuoc = [
 {
   "STT": "1",
   "ten_co_so": "Nhà thuốc  Tuyết Thảo 3",
   "dia_chi": "số 33 đường Nguyễn Huệ, phường 3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2838539",
   "Latitude": "105.7208581"
 },
 {
   "STT": "2",
   "ten_co_so": "Nhà thuốc  Khai Minh 2",
   "dia_chi": "số 42 đường Nguyễn Huệ, phường 3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2838539",
   "Latitude": "105.7208581"
 },
 {
   "STT": "3",
   "ten_co_so": "Nhà thuốc  Quốc Khánh",
   "dia_chi": "số 432 đường Võ Thị Sáu, phường 3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2882333",
   "Latitude": "105.7181391"
 },
 {
   "STT": "4",
   "ten_co_so": "Nhà thuốc  Vân Trang",
   "dia_chi": "số 195 đường Bà Triệu, phường 3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2875976",
   "Latitude": "105.724107"
 },
 {
   "STT": "5",
   "ten_co_so": "Nhà thuốc  Hồng Hà",
   "dia_chi": "số D 04/96 đường 23 tháng 8, phường 8, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2968652",
   "Latitude": "105.7181458"
 },
 {
   "STT": "6",
   "ten_co_so": "Nhà thuốc  Anh Lực",
   "dia_chi": "Số 98 Tỉnh lộ 38, khóm 7, Phường 5, thành phố Bạc Liêu, Tỉnh Bạc Liêu",
   "Longtitude": "9.2061818",
   "Latitude": "105.7348179"
 },
 {
   "STT": "7",
   "ten_co_so": "Nhà thuốc  Tuyết Anh",
   "dia_chi": "Nhà không số(thửa đất 882, tờ bản đồ số 3), Liên tỉnh lộ 38, Phường 5, thành phố Bạc Liêu, Tỉnh Bạc Liêu",
   "Longtitude": "9.2037994",
   "Latitude": "105.7292396"
 },
 {
   "STT": "8",
   "ten_co_so": "Nhà thuốc  Ánh Dương",
   "dia_chi": "Số 51A, đường Lý Thường Kiệt, khóm 4, Phường 3,thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2889538",
   "Latitude": "105.7251585"
 },
 {
   "STT": "9",
   "ten_co_so": "Nhà thuốc  An Kỳ",
   "dia_chi": "Số 429, đường 23/8, Phường 8, thành phố Bạc Liêu",
   "Longtitude": "9.2967004",
   "Latitude": "105.7026486"
 },
 {
   "STT": "10",
   "ten_co_so": "Nhà thuốc  Minh Hiếu",
   "dia_chi": "Số 346 Võ Thị Sáu, P.3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2882578",
   "Latitude": "105.7180262"
 },
 {
   "STT": "11",
   "ten_co_so": "Nhà thuốc  Trung Nghĩa",
   "dia_chi": "Số 27, đường Ninh Bình, Phường 2, thành phố Bạc Liêu, Tỉnh Bạc Liêu",
   "Longtitude": "9.2738687",
   "Latitude": "105.7409852"
 },
 {
   "STT": "12",
   "ten_co_so": "Nhà thuốc  Ngọc Yến",
   "dia_chi": "Số 37, đường Hà Huy Tập, Phường 3, thành phố Bạc Liêu, Tỉnh Bạc Liêu",
   "Longtitude": "9.2868248",
   "Latitude": "105.7245575"
 },
 {
   "STT": "13",
   "ten_co_so": "Nhà thuốc  Hòa Thành",
   "dia_chi": "Số 187A, đường Nguyễn Thị Minh Khai, Phường 5, thành phố Bạc Liêu, Tỉnh Bạc Liêu",
   "Longtitude": "9.2838615",
   "Latitude": "105.7285307"
 },
 {
   "STT": "14",
   "ten_co_so": "Nhà thuốc  Quốc Lợi",
   "dia_chi": "Số 179, đường Bà Triệu, Phường 3, thành phố Bạc Liêu, Tỉnh Bạc Liêu",
   "Longtitude": "9.2875976",
   "Latitude": "105.724107"
 },
 {
   "STT": "15",
   "ten_co_so": "Nhà thuốc  Xuân Hoàng",
   "dia_chi": "Số 89/53, đường Cách Mạng, Phường 1, thành phố Bạc Liêu, Tỉnh Bạc Liêu",
   "Longtitude": "9.3004123",
   "Latitude": "105.7303371"
 },
 {
   "STT": "16",
   "ten_co_so": "Quầy thuốc  Thiên Kim",
   "dia_chi": "Số 20 ấp Long Hậu, TT Phước Long, huyện Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4377778",
   "Latitude": "105.4630556"
 },
 {
   "STT": "17",
   "ten_co_so": "Quầy thuốc  Thanh Nhàn",
   "dia_chi": "Số 291, ấp Long Thành,TT Phước Long, huyện Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.36576509999999",
   "Latitude": "105.488828"
 },
 {
   "STT": "18",
   "ten_co_so": "Quầy thuốc  Phước Tài",
   "dia_chi": "Số 103A, ấp Nội Ô, thị trấn  Phước Long, huyện Phước Long",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "19",
   "ten_co_so": "Quầy thuốc  Kim Chi",
   "dia_chi": "Số 224B, ấp Nội Ô, thị trấn  Phước Long, huyện Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "21.0277644",
   "Latitude": "105.8341598"
 },
 {
   "STT": "20",
   "ten_co_so": "Quầy thuốc  Quốc Dũng",
   "dia_chi": "Ấp Vĩnh Hòa, xã Vĩnh Thanh, huyện Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.3536316",
   "Latitude": "105.522889"
 },
 {
   "STT": "21",
   "ten_co_so": "Quầy thuốc  Thanh Vân",
   "dia_chi": "196B, ấp Long Thành, TT Phước Long, huyện Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "22",
   "ten_co_so": "Quầy thuốc  Minh Hiếu",
   "dia_chi": "Số 121, ấp Sóc Đồn, xã Hưng Hội, huyện Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3372716",
   "Latitude": "105.7644596"
 },
 {
   "STT": "23",
   "ten_co_so": "Quầy thuốc  Mỹ Linh",
   "dia_chi": "Số 78 Ấp Thị Trấn A, thị trấn  Hòa Bình, huyện  Hòa Bình",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "24",
   "ten_co_so": "Quầy thuốc  Tuấn Khôi",
   "dia_chi": "Ấp 16B, xã Phong Tân, Thị xã Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.3017",
   "Latitude": "105.4403"
 },
 {
   "STT": "25",
   "ten_co_so": "Quầy thuốc  Thảo My",
   "dia_chi": "Số 222, K. 4, P.1, Thị xã Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.260136",
   "Latitude": "105.3753129"
 },
 {
   "STT": "26",
   "ten_co_so": "Quầy thuốc  Diễm Thy",
   "dia_chi": "Số 536, K.5, P,1, thị xã Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.260136",
   "Latitude": "105.3753129"
 },
 {
   "STT": "27",
   "ten_co_so": "Quầy thuốc   Kim Tuyến",
   "dia_chi": "Số 07, ấp Khúc Tréo A, xã Tân Phong, Thị xã Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.1986972",
   "Latitude": "105.3301616"
 },
 {
   "STT": "28",
   "ten_co_so": "Quầy thuốc  Ngọc Bo",
   "dia_chi": "Số 404, ấp Khúc Tréo B, xã Tân Phong, thị xã  Giá Rai,tỉnh Bạc Liêu",
   "Longtitude": "9.1963166",
   "Latitude": "105.3298112"
 },
 {
   "STT": "29",
   "ten_co_so": "Quầy thuốc  Thanh Hải",
   "dia_chi": "Số 411 Q.lộ 1A, ấp khúc Tréo A, xã Tân Phong, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.1989667",
   "Latitude": "105.3372637"
 },
 {
   "STT": "30",
   "ten_co_so": "Thuốc đông y  Tế Nhơn An 222",
   "dia_chi": "Ấp Nhàn Dân A, xã Tân Phong, TX Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.18879999999999",
   "Latitude": "105.308593"
 },
 {
   "STT": "31",
   "ten_co_so": "Thuốc đông y  Trường Ngươn Đường",
   "dia_chi": "Số 398, đường 30/4, khóm 2, phường Hộ Phòng, TX Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "32",
   "ten_co_so": "Quầy thuốc  Hải Yến ",
   "dia_chi": "Số 2, ấp Khúc Tréo B, xã Tân Phong, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.1963166",
   "Latitude": "105.3298112"
 },
 {
   "STT": "33",
   "ten_co_so": "Quầy thuốc  Thanh Hoa",
   "dia_chi": "Kios 05, đường Huỳnh Hoàng Hùng, K. 2, P. Hộ Phòng, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "34",
   "ten_co_so": "Quầy thuốc  Quang thái",
   "dia_chi": "Số 144, khóm 1, P. Hộ Phòng, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "35",
   "ten_co_so": "Quầy thuốc  Ánh Hồng",
   "dia_chi": "Số 273E Phan Thanh Giản, K. 5, thị xã  Giá Rai",
   "Longtitude": "9.260136",
   "Latitude": "105.3753129"
 },
 {
   "STT": "36",
   "ten_co_so": "Quầy thuốc  Ngọc Mai",
   "dia_chi": "Số 2, Khóm 2, P. Hộ Phòng, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2259938",
   "Latitude": "105.4124112"
 },
 {
   "STT": "37",
   "ten_co_so": "Quầy thuốc  Thanh Đạm",
   "dia_chi": "Số 450 ấp 2, xã Phong Thạnh Đông A, thị xã  Giá Rai",
   "Longtitude": "9.2735574",
   "Latitude": "105.5064087"
 },
 {
   "STT": "38",
   "ten_co_so": "Quầy thuốc  Minh Nguyên",
   "dia_chi": "Số 34, K. 2, P. Hộ Phòng, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "39",
   "ten_co_so": "Quầy thuốc  Quốc Dũng",
   "dia_chi": "666, âp An Khoa, xã Vĩnh Mỹ B, huyện Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.28581889999999",
   "Latitude": "105.5974766"
 },
 {
   "STT": "40",
   "ten_co_so": "Quầy thuốc  Cao Thành ",
   "dia_chi": "Ấp Vĩnh Lạc, xã Vĩnh Thịnh, huyện Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.1886028",
   "Latitude": "105.6060661"
 },
 {
   "STT": "41",
   "ten_co_so": "Quầy thuốc  Mộng Nghi",
   "dia_chi": "Ấp 15, xã Vĩnh Mỹ B, huyện Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.29349519999999",
   "Latitude": "105.5648804"
 },
 {
   "STT": "42",
   "ten_co_so": "Quầy thuốc  Quốc Minh",
   "dia_chi": "Số 36, ấp Xóm Lớn B, xã Vĩnh Mỹ A, huyện Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.23258549999999",
   "Latitude": "105.5826123"
 },
 {
   "STT": "43",
   "ten_co_so": "Quầy thuốc  Ngọc Thảo ",
   "dia_chi": "Ấp Bờ Cảng, xã Điền Hải, huyện Đông Hải,tỉnh Bạc Liêu",
   "Longtitude": "9.108406",
   "Latitude": "105.489159"
 },
 {
   "STT": "44",
   "ten_co_so": "Quầy thuốc  Hồng Thảnh",
   "dia_chi": "Ấp Diêm Điền, Xã Điền Hải, huyện Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1056847",
   "Latitude": "105.4919293"
 },
 {
   "STT": "45",
   "ten_co_so": "Quầy thuốc  Cầm Nên",
   "dia_chi": "Ấp Diêm Điền, Xã Điền Hải, huyện Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1056847",
   "Latitude": "105.4919293"
 },
 {
   "STT": "46",
   "ten_co_so": "Quầy thuốc  Thu Ngân",
   "dia_chi": "Số 112, ấp Cây Thẻ, xã Định Thành, huyện Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1323056",
   "Latitude": "105.2955575"
 },
 {
   "STT": "47",
   "ten_co_so": "Quầy thuốc  Tư Kỳ",
   "dia_chi": "Ấp Văn Đức A, xã An Trạch,huyện Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1763456",
   "Latitude": "105.3950939"
 },
 {
   "STT": "48",
   "ten_co_so": "Quầy thuốc  Nhã Uyên",
   "dia_chi": "Số 332 ấp Mỹ Điền, xã Long Điền Đông A, huyện Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.21670649999999",
   "Latitude": "105.5361154"
 },
 {
   "STT": "49",
   "ten_co_so": "Quầy thuốc  Hoàng Tỷ",
   "dia_chi": "Số 339 Ấp 2, thị trấn  Gành Hào, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.0349627",
   "Latitude": "105.4282431"
 },
 {
   "STT": "50",
   "ten_co_so": "Quầy thuốc  Lâm Văn Năm",
   "dia_chi": "Số 41B ấp Diêm Điền, xã Điền Hải, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.10787869999999",
   "Latitude": "105.488828"
 },
 {
   "STT": "51",
   "ten_co_so": "Quầy thuốc  Kim Thúy",
   "dia_chi": "Số 358, ấp Cái Tràm A1, xã Long Thạnh, huyện Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.2861259",
   "Latitude": "105.6822968"
 },
 {
   "STT": "52",
   "ten_co_so": "Quầy thuốc  Trúc Xuân",
   "dia_chi": "Số 194, ấp Trà Ban 2, xã Châu Hưng A, huyện Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.37675679999999",
   "Latitude": "105.7233814"
 },
 {
   "STT": "53",
   "ten_co_so": "Quầy thuốc  Thúy Nhi",
   "dia_chi": "ấp Xẻo Chích, thị trấn Châu Hưng, huyện Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3333333",
   "Latitude": "105.7166667"
 },
 {
   "STT": "54",
   "ten_co_so": "Quầy thuốc  Toàn Tâm",
   "dia_chi": " ấp Trà Ban I, xã Châu Hưng A, huyện Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.37768829999999",
   "Latitude": "105.7135109"
 },
 {
   "STT": "55",
   "ten_co_so": "Quầy thuốc  Cẩm Tấn",
   "dia_chi": "Quốc lộ 1A, ấp Cái Dầy, thị trấn Châu Hưng, huyện Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3432558",
   "Latitude": "105.7149569"
 },
 {
   "STT": "56",
   "ten_co_so": "Quầy thuốc  Gia Huy",
   "dia_chi": "Số 90, ấp Tam Hưng, xã Vĩnh Hưng, huyện Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3871631",
   "Latitude": "105.6119301"
 },
 {
   "STT": "57",
   "ten_co_so": "Quầy thuốc  Út Huệ",
   "dia_chi": "Số 134, ấp Cái Dầy, thị trấn Châu Hưng, huyện Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3432558",
   "Latitude": "105.7149569"
 },
 {
   "STT": "58",
   "ten_co_so": "Quầy thuốc  Hồng Anh",
   "dia_chi": "Số 254, ấp Phước Thạnh 1, xã Long Thạnh, huyện Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3092534",
   "Latitude": "105.6705801"
 },
 {
   "STT": "59",
   "ten_co_so": "Quầy thuốc  Tuấn Đạt",
   "dia_chi": "Số 037, Khu IB, ấp Nội Ô, thị trấn Ngan Dừa, huyện Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "60",
   "ten_co_so": "Quầy thuốc  Minh Hiệp 1",
   "dia_chi": "Số 240 khu vực II, ấp Nội Ô, thị trấn Ngan Dừa, huyện Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "61",
   "ten_co_so": "Quầy thuốc  Kim Dững",
   "dia_chi": "ấp Nội Ô, thị trấn  Ngan Dừa, huyện  Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "62",
   "ten_co_so": "Quầy thuốc  Văn Chiến",
   "dia_chi": "Số 334, ấp Vĩnh Hòa, xã Vĩnh Thanh, Phước Long, Bạc Liêu",
   "Longtitude": "9.36576509999999",
   "Latitude": "105.488828"
 },
 {
   "STT": "63",
   "ten_co_so": "Quầy thuốc  Phước Vũ",
   "dia_chi": "Số 73, ấp Phước Thành, xã Phước Long, huyện  Phước Long, Bạc Liêu",
   "Longtitude": "9.4130056",
   "Latitude": "105.3950939"
 },
 {
   "STT": "64",
   "ten_co_so": "Quầy thuốc  Dương Chiêu",
   "dia_chi": "Số 118/B, ấp Nội Ô, thị trấn  Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "65",
   "ten_co_so": "Quầy thuốc  Thanh Trung",
   "dia_chi": "Số 328/B, ấp Nội Ô, thị trấn  Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "66",
   "ten_co_so": "Quầy thuốc  Ngọc Nhung",
   "dia_chi": "Số 1/114, ấp Vĩnh An, xã Vĩnh Trạch, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.37853099999999",
   "Latitude": "105.3946"
 },
 {
   "STT": "67",
   "ten_co_so": "Nhà thuốc  Bà Triệu",
   "dia_chi": "31 Bà Triệu - P3-thành phố.  Bạc Liêu",
   "Longtitude": "9.2877662",
   "Latitude": "105.7242412"
 },
 {
   "STT": "68",
   "ten_co_so": "Nhà thuốc  Thế Cường",
   "dia_chi": "Số 125 đường Cách Mạng, K. 10, P. 1, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2940027",
   "Latitude": "105.7215663"
 },
 {
   "STT": "69",
   "ten_co_so": "Nhà thuốc   Thanh Long",
   "dia_chi": "171, Võ Thị Sáu, P.7, thành phố Bạc Liêu",
   "Longtitude": "9.2912161",
   "Latitude": "105.7139066"
 },
 {
   "STT": "70",
   "ten_co_so": "Nhà thuốc   Hiền Khanh",
   "dia_chi": "Số 29 đường Cách Mạng, K. 10, P. 1, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2957663",
   "Latitude": "105.7363325"
 },
 {
   "STT": "71",
   "ten_co_so": "Nhà thuốc  Khánh Trung",
   "dia_chi": "101-Trần Huỳnh-Phường 7-thành phố Bạc Liêu",
   "Longtitude": "9.2936646",
   "Latitude": "105.7202339"
 },
 {
   "STT": "72",
   "ten_co_so": "Quầy thuốc  Minh Tuấn",
   "dia_chi": "Ấp Vĩnh Mẫu, xã Vĩnh Hậu, huyện Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.2131068",
   "Latitude": "105.6588485"
 },
 {
   "STT": "73",
   "ten_co_so": "Quầy thuốc  Cẩm Tú",
   "dia_chi": "Số 688, ấp B1, thị trấn  Hòa Bình, huyện Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "74",
   "ten_co_so": "Quầy thuốc  Thành Đạt",
   "dia_chi": "Số 179 Quốc lộ 1A, ấp thị trấn A, thị trấn  Hòa Bình, huyện Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "75",
   "ten_co_so": "Quầy thuốc  Phương Vy",
   "dia_chi": "Ấp Long Thành, thị trấn  Phước Long, huyện Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4314017",
   "Latitude": "105.4612324"
 },
 {
   "STT": "76",
   "ten_co_so": "Quầy thuốc  Hồng Ảnh",
   "dia_chi": "Ấp Tường Tư, xã Hưng Phú, huyện Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.3858316",
   "Latitude": "105.518982"
 },
 {
   "STT": "77",
   "ten_co_so": "Quầy thuốc  Xuân Lộc",
   "dia_chi": "Ấp Nội Ô, thị trấn Phước Long, Phước Long, Bạc Liêu",
   "Longtitude": "9.4478981",
   "Latitude": "105.4616474"
 },
 {
   "STT": "78",
   "ten_co_so": "Quầy thuốc  Lynh Uyên",
   "dia_chi": "Số 511, Phan Thanh Giản, Ấp 5,  thị trấn  Giá Rai, huyện Giá Rai",
   "Longtitude": "9.2388149",
   "Latitude": "105.4586323"
 },
 {
   "STT": "79",
   "ten_co_so": "Quầy thuốc  Tuấn Tú",
   "dia_chi": "464 ấp Nhàn Dân A, xã Tân Phong, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2213584",
   "Latitude": "105.348246"
 },
 {
   "STT": "80",
   "ten_co_so": "Nhà thuốc Bệnh Viện  Bệnh viện",
   "dia_chi": "K.1, P. 1, thị xã  Giá Rai, Bạc Liêu",
   "Longtitude": "9.260136",
   "Latitude": "105.3753129"
 },
 {
   "STT": "81",
   "ten_co_so": "Nhà Thuốc  Huỳnh Tuấn",
   "dia_chi": "Số 110, khóm 2, P. Hộ Phòng, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "82",
   "ten_co_so": "Quầy thuốc   Liễu",
   "dia_chi": "Kios 37-39, đường Nguyễn Quốc Hương, K.2, P. Hộ Phòng, thị xã  Giá Rai, Bạc Liêu",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "83",
   "ten_co_so": "Quầy thuốc  Minh Quyền",
   "dia_chi": "Số 656, Khóm 2, P. Láng Tròn, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2735574",
   "Latitude": "105.5064087"
 },
 {
   "STT": "84",
   "ten_co_so": "Quầy thuốc  Tố Liên",
   "dia_chi": "Số 453, K. 2, P. Láng Tròn, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2735574",
   "Latitude": "105.5064087"
 },
 {
   "STT": "85",
   "ten_co_so": "Cơ sở bán buôn thuốc thành phẩm  Thái Nhựt",
   "dia_chi": "Số 07 Lầu 1, đường Tôn Đức Thắng, P. 1, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2984779",
   "Latitude": "105.7356342"
 },
 {
   "STT": "86",
   "ten_co_so": "Quầy thuốc  Thành Huệ",
   "dia_chi": "Số 330 Ấp Nội ô, thị trấn  Ngan Dừa, huyện  Hồng Dân",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "87",
   "ten_co_so": "Quầy thuốc  Nguyễn Thanh Tùng",
   "dia_chi": "111, ấp Ninh Thạnh, xã Ninh Quới A, Hồng Dân, Bạc Liêu",
   "Longtitude": "9.5668968",
   "Latitude": "105.4529572"
 },
 {
   "STT": "88",
   "ten_co_so": "Quầy thuốc  Thanh Bình II",
   "dia_chi": "Ki ốt B ấp Nội Ô, thị trấn  Ngan Dừa, huyện  Hồng Dân",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "89",
   "ten_co_so": "Nhà thuốc  Hiếu Dân",
   "dia_chi": "30,  Hà Huy Tập,  P.3 thành phố Bạc Liêu",
   "Longtitude": "9.2870607",
   "Latitude": "105.7244207"
 },
 {
   "STT": "90",
   "ten_co_so": "Nhà thuốc  Hữu Tâm",
   "dia_chi": "Số 07, Đường Hà Huy Tập,  Phường 3, thành phố Bạc Liêu",
   "Longtitude": "9.2868248",
   "Latitude": "105.7245575"
 },
 {
   "STT": "91",
   "ten_co_so": "Nhà thuốc  Anh Thư",
   "dia_chi": "Số 208 Võ Thị Sáu, P.3, thành phố Bạc Liêu",
   "Longtitude": "9.29073629999999",
   "Latitude": "105.7145578"
 },
 {
   "STT": "92",
   "ten_co_so": "Quầy thuốc  Minh Đức",
   "dia_chi": "Số 27, ấp Nhà Việc, xã Châu Thới, Vĩnh Lợi, Bạc Liêu",
   "Longtitude": "9.358423",
   "Latitude": "105.652983"
 },
 {
   "STT": "93",
   "ten_co_so": "Quầy thuốc  Việt Hằng",
   "dia_chi": "Số 131, ấp Cái Dầy, TT Châu Hưng, Vĩnh Lợi, Bạc Liêu",
   "Longtitude": "9.3432558",
   "Latitude": "105.7149569"
 },
 {
   "STT": "94",
   "ten_co_so": "Quầy thuốc  Út Huệ 2",
   "dia_chi": "Số 273, ấp Cái Dầy, thị trấn  Châu Hưng, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3432558",
   "Latitude": "105.7149569"
 },
 {
   "STT": "95",
   "ten_co_so": "Quầy thuốc  Dương Quân",
   "dia_chi": "Số 40, ấp Xẻo Chích, thị trấn  Châu Hưng, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3388612",
   "Latitude": "105.7292491"
 },
 {
   "STT": "96",
   "ten_co_so": "Quầy thuốc  Minh Minh",
   "dia_chi": "Ấp Gò Cát, xã Điền Hải, Đông Hải, Bạc Liêu",
   "Longtitude": "9.1058995",
   "Latitude": "105.491815"
 },
 {
   "STT": "97",
   "ten_co_so": "Quầy thuốc  Tấn Đạt",
   "dia_chi": "Số 142, ấp Bờ Cảng, xã Điền Hải, Bạc Liêu",
   "Longtitude": "9.10787869999999",
   "Latitude": "105.488828"
 },
 {
   "STT": "98",
   "ten_co_so": "Quầy thuốc  Đạt Sang II",
   "dia_chi": "309 ấp 4 thị trấn Gành Hào, huyện Đông Hải",
   "Longtitude": "9.0383639",
   "Latitude": "105.4199115"
 },
 {
   "STT": "99",
   "ten_co_so": "Quầy thuốc  Long Điền",
   "dia_chi": "ấp Cây Giang, xã Long Điền, Đông Hải, Bạc Liêu",
   "Longtitude": "9.1676975",
   "Latitude": "105.4481035"
 },
 {
   "STT": "100",
   "ten_co_so": "Quầy thuốc  Tuấn Phong",
   "dia_chi": "Số 74, ấp 3, thị trấn  Gành Hào, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.0349627",
   "Latitude": "105.4282431"
 },
 {
   "STT": "101",
   "ten_co_so": "Quầy thuốc  Thanh Nga",
   "dia_chi": "Số 15, ấp Thị Trấn A, thị trấn  Hòa Bình, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "102",
   "ten_co_so": "Quầy thuốc  Ninh Đẹp",
   "dia_chi": "Số 278B, ấp Nội Ô, thị trấn  Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "103",
   "ten_co_so": "Quầy thuốc  Số 20",
   "dia_chi": "Số 346B, ấp Nội Ô, thị trấn  Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "104",
   "ten_co_so": "Quầy thuốc  Hiếu Lộc",
   "dia_chi": "Số 389, ấp Nhàn Dân A, xã Tân Phong, huyện Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2213584",
   "Latitude": "105.348246"
 },
 {
   "STT": "105",
   "ten_co_so": "Quầy thuốc  Út An",
   "dia_chi": "Số 120 Ấp Nội Ô, thị trấn  Ngan Dừa, huyện  Hồng Dân",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "106",
   "ten_co_so": "Quầy thuốc  Kim Kha",
   "dia_chi": "Số 2/444, ấp Biển Đông B, xã Vĩnh Trạch Đông, thành phốBL, Bạc Liêu",
   "Longtitude": "9.2661286",
   "Latitude": "105.7938069"
 },
 {
   "STT": "107",
   "ten_co_so": "Nhà thuốc  Tường Nhi",
   "dia_chi": "Số 309 Cao Văn Lầu, P.5, thành phố Bạc Liêu",
   "Longtitude": "9.2745549",
   "Latitude": "105.7296603"
 },
 {
   "STT": "108",
   "ten_co_so": "Nhà thuốc   Thanh Vũ",
   "dia_chi": "26A Bà Triệu, P3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2886717",
   "Latitude": "105.7262786"
 },
 {
   "STT": "109",
   "ten_co_so": "Nhà thuốc  Linh Đan",
   "dia_chi": "Số 7/3, Đường Nguyễn Tất Thành, P.7, thành phố Bạc Liêu",
   "Longtitude": "9.2940027",
   "Latitude": "105.7215663"
 },
 {
   "STT": "110",
   "ten_co_so": "Quầy thuốc  Ngọc Nhi",
   "dia_chi": "Số 36 ấp Cù Lao, xã Hưng Hội, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3028015",
   "Latitude": "105.744701"
 },
 {
   "STT": "111",
   "ten_co_so": "Quầy thuốc   Khánh Bình",
   "dia_chi": "Ấp Chùa Phật, thị trấn Hòa Bình, huyện Hòa Bình, Bạc Liêu",
   "Longtitude": "9.2739657",
   "Latitude": "105.6324959"
 },
 {
   "STT": "112",
   "ten_co_so": "Quầy thuốc  Thống Thắng ",
   "dia_chi": "142 ấp Vĩnh Lạc, xã Vĩnh Thịnh, Hòa Bình, Bạc Liêu",
   "Longtitude": "9.1886028",
   "Latitude": "105.6060661"
 },
 {
   "STT": "113",
   "ten_co_so": "Quầy thuốc  Tuấn Đạt",
   "dia_chi": "ấp Xóm Lớn B, xã Vĩnh Mỹ A, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.23258549999999",
   "Latitude": "105.5826123"
 },
 {
   "STT": "114",
   "ten_co_so": "Quầy thuốc   Huỳnh Châu Thiệt",
   "dia_chi": "Số 253, K. 1, P. 1, thị xã Giá Rai,tỉnh Bạc Liêu",
   "Longtitude": "9.260136",
   "Latitude": "105.3753129"
 },
 {
   "STT": "115",
   "ten_co_so": "Quầy thuốc  Phúc Hậu",
   "dia_chi": "số 20 ấp Xóm Mới, xã Tân Thạnh, TX Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2090313",
   "Latitude": "105.2604409"
 },
 {
   "STT": "116",
   "ten_co_so": "Quầy thuốc   Khánh Phong",
   "dia_chi": "Số 274, Q.Lộ 1A, K. 2, P.1, thị xã Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "17.5884801",
   "Latitude": "106.5333977"
 },
 {
   "STT": "117",
   "ten_co_so": "Quầy thuốc   Đức Vương",
   "dia_chi": "Số 15 Chợ Mới, phường Hộ Phòng, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "10.4825247",
   "Latitude": "105.4771084"
 },
 {
   "STT": "118",
   "ten_co_so": "Quầy thuốc  Diệu Hùng",
   "dia_chi": "Số 125, ấp Phước Thành, xã Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.3678476",
   "Latitude": "105.4419546"
 },
 {
   "STT": "119",
   "ten_co_so": "Quầy thuốc  Thoại Trang",
   "dia_chi": "Số 96B, ấp Nội Ô, thị trấn  Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "120",
   "ten_co_so": "Quầy thuốc  Số 11",
   "dia_chi": "Số 01 ấp Ninh Thạnh, Ninh Quới A, huyện  Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.51553049999999",
   "Latitude": "105.5122694"
 },
 {
   "STT": "121",
   "ten_co_so": "Quầy thuốc  Thiện Phước ",
   "dia_chi": "Số 46, ấp Diêm Điền, xã Điền Hải, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.10787869999999",
   "Latitude": "105.488828"
 },
 {
   "STT": "122",
   "ten_co_so": "Nhà thuốc  Khai Minh",
   "dia_chi": "23 Hà Huy Tập, P.3,thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.28713769999999",
   "Latitude": "105.7244861"
 },
 {
   "STT": "123",
   "ten_co_so": "Nhà thuốc  Thuận An",
   "dia_chi": "Số 360 Võ Thị Sáu, P.3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2879718",
   "Latitude": "105.718448"
 },
 {
   "STT": "124",
   "ten_co_so": "Nhà thuốc  Duy Luân",
   "dia_chi": "Số 270A/5 Cao Văn Lầu, Khóm Đầu Lộ A, P. Nhà Mát, thành phố Bạc Liêu",
   "Longtitude": "9.2836381",
   "Latitude": "105.7254046"
 },
 {
   "STT": "125",
   "ten_co_so": "Nhà thuốc  Thảo Minh",
   "dia_chi": "Số 2/47, Tỉnh lộ 38, K5, P5, thành phốBL, Bạc Liêu",
   "Longtitude": "9.2061818",
   "Latitude": "105.7348179"
 },
 {
   "STT": "126",
   "ten_co_so": "Nhà thuốc  Tuyết Thảo 1",
   "dia_chi": "Số 464 Võ Thị Sáu, P.3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.28599",
   "Latitude": "105.7206622"
 },
 {
   "STT": "127",
   "ten_co_so": "Cơ sở bán lẻ thuốc dược liệu, thuốc cổ truyền  Hậu Sanh Đường",
   "dia_chi": "Số 63 Lê Văn Duyệt, P. 3, thành phố Bạc Liêu",
   "Longtitude": "9.288302",
   "Latitude": "105.7248716"
 },
 {
   "STT": "128",
   "ten_co_so": "Nhà thuốc  Mai Yên",
   "dia_chi": "Số 74 Hòa Bình, P.3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.28800429999999",
   "Latitude": "105.7221896"
 },
 {
   "STT": "129",
   "ten_co_so": "Quầy thuốc  Ý Niệm",
   "dia_chi": "Số 204, ấp Cái Dầy, thị trấn  Châu Hưng, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3432558",
   "Latitude": "105.7149569"
 },
 {
   "STT": "130",
   "ten_co_so": "Quầy thuốc  Đăng Khoa",
   "dia_chi": "Ấp Thị Trấn A1, thị trấn  Hòa Bình, huyện  Hòa Bình",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "131",
   "ten_co_so": "Quầy thuốc  Kiên Nhẫn",
   "dia_chi": "Số 524, Khóm 1, P.1, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.260136",
   "Latitude": "105.3753129"
 },
 {
   "STT": "132",
   "ten_co_so": "Quầy thuốc  Thiên Phú",
   "dia_chi": "Số 100, ấp Vĩnh Hòa, xã Vĩnh Thanh, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.36576509999999",
   "Latitude": "105.488828"
 },
 {
   "STT": "133",
   "ten_co_so": "Quầy thuốc   Thanh Liêm",
   "dia_chi": "Số 417, ấp Thọ Tiền, xã Phước Long,  Phước Long, Bạc Liêu",
   "Longtitude": "9.4130056",
   "Latitude": "105.3950939"
 },
 {
   "STT": "134",
   "ten_co_so": "Quầy thuốc   Thuận Hưng",
   "dia_chi": "Số 37, ấp Phước Thành, xã Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.36576509999999",
   "Latitude": "105.488828"
 },
 {
   "STT": "135",
   "ten_co_so": "Quầy thuốc  Lý Yến Nhi",
   "dia_chi": "Số 418, ấp Phước Thọ Tiền, xã Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4130056",
   "Latitude": "105.3950939"
 },
 {
   "STT": "136",
   "ten_co_so": "Quầy thuốc  Trương Tuyết Vân",
   "dia_chi": "Số 338B, ấp Nội Ô, thị trấn  Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "137",
   "ten_co_so": "Quầy thuốc  Quang Huy",
   "dia_chi": "Số 66 ấp cầu Đỏ, xã Vĩnh Lộc, huyện  hồng Dân, Bạc Liêu",
   "Longtitude": "9.5639773",
   "Latitude": "105.3950939"
 },
 {
   "STT": "138",
   "ten_co_so": "Cơ sở bán lẻ thuốc dược liệu, thuốc cổ truyền  Tám Tỵ",
   "dia_chi": "Số 31 Ấp Ninh Thạnh, xã Ninh Quới A, huyện Hồng Dân",
   "Longtitude": "9.51553049999999",
   "Latitude": "105.5122694"
 },
 {
   "STT": "139",
   "ten_co_so": "Quầy thuốc  Quốc Phong",
   "dia_chi": "Số 117, ấp Ninh Thạnh, xã Ninh Quới A, huyện  Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.51553049999999",
   "Latitude": "105.5122694"
 },
 {
   "STT": "140",
   "ten_co_so": "Quầy thuốc  Trọng Nhi",
   "dia_chi": "Số 7, Ấp 3, thị trấn  Gành Hào, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.0268594",
   "Latitude": "105.4195905"
 },
 {
   "STT": "141",
   "ten_co_so": "Quầy thuốc  Cúc Hương",
   "dia_chi": "Số 191 Ngọc Điền, ấp 2, thị trấn  Gành Hào, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.0267018",
   "Latitude": "105.4191388"
 },
 {
   "STT": "142",
   "ten_co_so": "Quầy thuốc  Hồng Công",
   "dia_chi": "Số 47, ấp Diêm Điền, xã Điền Hải, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.10787869999999",
   "Latitude": "105.488828"
 },
 {
   "STT": "143",
   "ten_co_so": "Quầy thuốc  Hồng Vân",
   "dia_chi": "Số 78, ấp Nhà Lầu 2, xã Ninh Thạnh Lợi A, huyện Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.4376772",
   "Latitude": "105.3248269"
 },
 {
   "STT": "144",
   "ten_co_so": "Nhà thuốc  Nhà thuốc Trung tâm Y tế huyện Hồng Dân",
   "dia_chi": "Số 01 Trần Hưng Đạo, Ấp Nội Ô, thị trấn  Ngan Dừa, huyện  Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "145",
   "ten_co_so": "Quầy thuốc  Thành To",
   "dia_chi": "Số 123, ấp Mỹ Tường 1, xã Hưng Phú, Phước Long, Bạc Liêu",
   "Longtitude": "9.4113246",
   "Latitude": "105.5532993"
 },
 {
   "STT": "146",
   "ten_co_so": "Quầy thuốc  Công Nguyên",
   "dia_chi": "Số 221A, ấp Nội Ô, thị trấn  Phước Long, huyện  Phước Long",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "147",
   "ten_co_so": "Quầy thuốc  Ngọc Minh",
   "dia_chi": "50 Ấp 12, xã vĩnh Hậu A, huyện Hòa Bình, Bạc Liêu",
   "Longtitude": "9.23301459999999",
   "Latitude": "105.6940454"
 },
 {
   "STT": "148",
   "ten_co_so": "Quầy thuốc  Phương Anh",
   "dia_chi": "Số 01 Lô B, TTTM, Ấp Thị Trấn A, thị trấn  Hòa Bình, huyện  Hòa Bình",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "149",
   "ten_co_so": "Quầy thuốc   Thành Phát",
   "dia_chi": "144 ấp Vĩnh Lạc, xã Vĩnh Thịnh, Hòa Bình, Bạc Liêu",
   "Longtitude": "9.1886028",
   "Latitude": "105.6060661"
 },
 {
   "STT": "150",
   "ten_co_so": "Quầy thuốc  Ái Trân",
   "dia_chi": "Số 19, ấp 21, xã Minh Diệu, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.3390177",
   "Latitude": "105.6060661"
 },
 {
   "STT": "151",
   "ten_co_so": "Quầy thuốc  Bích Trân",
   "dia_chi": "Ấp Thị Trấn B, thị trấn  Hòa Bình, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "152",
   "ten_co_so": "Nhà Thuốc  Cẩm Duyên",
   "dia_chi": "Số 323, K. 3, P. Láng Tròn, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2735574",
   "Latitude": "105.5064087"
 },
 {
   "STT": "153",
   "ten_co_so": "Quầy thuốc  Minh Quân",
   "dia_chi": "Số 218 K. 5, P. Hộ Phòng, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "154",
   "ten_co_so": "Quầy thuốc  Phước Thịnh",
   "dia_chi": "Số 12, P. Hộ Phòng, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "155",
   "ten_co_so": "Quầy thuốc  Việt Thắng",
   "dia_chi": "Số 08 Chợ Mới,K. 2, P. Hộ Phòng, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "10.4825247",
   "Latitude": "105.4771084"
 },
 {
   "STT": "156",
   "ten_co_so": "Quầy thuốc  Việt Thành",
   "dia_chi": "Số 40 ấp Tam Hưng, xã Vĩnh Hưng, huyện  Vĩnh Lợi, Bạc Liêu",
   "Longtitude": "9.3871631",
   "Latitude": "105.6119301"
 },
 {
   "STT": "157",
   "ten_co_so": "Quầy thuốc  Ngọc Giàu",
   "dia_chi": "ấp Trung Hưng, xã Vĩnh Hưng A, huyện Vĩnh Lợi, T. Bạc Liêu",
   "Longtitude": "9.3871631",
   "Latitude": "105.6119301"
 },
 {
   "STT": "158",
   "ten_co_so": "Quầy thuốc  Song Duy",
   "dia_chi": "Số 104, ấp Cái Dầy, thị trấn  Châu Hưng, huyện  Vĩnh Lợi",
   "Longtitude": "9.3432558",
   "Latitude": "105.7149569"
 },
 {
   "STT": "159",
   "ten_co_so": "Quầy thuốc  Thanh Tú",
   "dia_chi": "ấp Nước Mặn, xã Hưng Hội, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.336645",
   "Latitude": "105.76416"
 },
 {
   "STT": "160",
   "ten_co_so": "Quầy thuốc  Thanh Tú",
   "dia_chi": "Số 290, ấp Tam Hưng, xã Vĩnh Hưng, huyện  Vĩnh Lợi, Bạc Liêu",
   "Longtitude": "9.3871631",
   "Latitude": "105.6119301"
 },
 {
   "STT": "161",
   "ten_co_so": "Quầy thuốc  Hoàng Siêng",
   "dia_chi": "Ấp Lung Chim, xã Định Thành, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1323056",
   "Latitude": "105.2955575"
 },
 {
   "STT": "162",
   "ten_co_so": "Nhà thuốc  Minh Khuê",
   "dia_chi": "Số 97 Bà Triệu, Phường 3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.28718299999999",
   "Latitude": "105.7232618"
 },
 {
   "STT": "163",
   "ten_co_so": "Nhà thuốc  Song nghi 1",
   "dia_chi": "Số 133 Trần Huỳnh, P.7, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2939884",
   "Latitude": "105.7211467"
 },
 {
   "STT": "164",
   "ten_co_so": "Quầy thuốc  Hữu Đạt",
   "dia_chi": " Số 01/766 ấp Vĩnh An, xã Vĩnh Trạch, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.37853099999999",
   "Latitude": "105.3946"
 },
 {
   "STT": "165",
   "ten_co_so": "Nhà thuốc  Thanh Tuấn",
   "dia_chi": "Số 6/379 đường Tỉnh lộ 38, K. 8, P. 5, thành phố Bạc Liêu",
   "Longtitude": "9.2037994",
   "Latitude": "105.7292396"
 },
 {
   "STT": "166",
   "ten_co_so": "Nhà thuốc  Tú Phương",
   "dia_chi": "Số 5/126 Cầu Kè, K. 5, P. 2, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2800023",
   "Latitude": "105.7210487"
 },
 {
   "STT": "167",
   "ten_co_so": "Nhà thuốc  Hồng Nga",
   "dia_chi": "Số 57A Hai Bà Trưng, P.3, thành phố Bạc Liêu",
   "Longtitude": "9.2872006",
   "Latitude": "105.7264673"
 },
 {
   "STT": "168",
   "ten_co_so": "Quầy thuốc  Hai Thắng",
   "dia_chi": "Số 38, Ấp 4, xã Phong Thạnh Tây B, huyện Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.332002",
   "Latitude": "105.2779983"
 },
 {
   "STT": "169",
   "ten_co_so": "Quầy thuốc  Số 28",
   "dia_chi": "175A Ấp Nội Ô, thị trấn  Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "170",
   "ten_co_so": "Quầy thuốc  Trần Huỳnh",
   "dia_chi": "Số 11, ấp 4, Xã Phong Thạnh Tây B, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.332002",
   "Latitude": "105.2779983"
 },
 {
   "STT": "171",
   "ten_co_so": "Quầy thuốc  Mỹ Tú",
   "dia_chi": "09 A ấp Long Hòa, thị trấn  Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.43951429999999",
   "Latitude": "105.462028"
 },
 {
   "STT": "172",
   "ten_co_so": "Quầy thuốc  Diên An",
   "dia_chi": "Lô 20, Đường Lê Duẩn, TTTM huyện Hồng Dân",
   "Longtitude": "9.55390289999999",
   "Latitude": "105.45205"
 },
 {
   "STT": "173",
   "ten_co_so": "Quầy thuốc  Phú Quý",
   "dia_chi": "Ấp Ninh Thạnh Tây, xã Ninh Thạnh Lợi, huyện  Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.48890879999999",
   "Latitude": "105.3537812"
 },
 {
   "STT": "174",
   "ten_co_so": "Quầy thuốc  Minh Hiệp",
   "dia_chi": "Lô 6C Nguyễn Thị Minh Khai, ấp Nội Ô, thị trấn  Ngan Dừa, huyện  Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "175",
   "ten_co_so": "Quầy thuốc  Ngọc Nang",
   "dia_chi": " Kios 10BẤp Nội Ô, thị Trấn Ngan Dừa, huyện  Hồng Dân,tỉnh Bạc Liêu",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "176",
   "ten_co_so": "Quầy thuốc  Hồng Sơn",
   "dia_chi": "Số 85B, ấp Nội Ô, Thị trấn Ngan Dừa, huyện  Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "177",
   "ten_co_so": "Quầy thuốc  Sơn Ca",
   "dia_chi": "Số 151 ấp Nhàn Dân A, xã Tân Phong, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2213584",
   "Latitude": "105.348246"
 },
 {
   "STT": "178",
   "ten_co_so": "Quầy thuốc  Phương Thảo",
   "dia_chi": "Số 35 ấp Giồng Bướm A, xã Châu Thới, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3601976",
   "Latitude": "105.6548624"
 },
 {
   "STT": "179",
   "ten_co_so": "Quầy thuốc  Thanh Tho",
   "dia_chi": "Số 68 ấp Thông Lưu, thị trấn  Châu Hưng, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3388612",
   "Latitude": "105.7292491"
 },
 {
   "STT": "180",
   "ten_co_so": "Quầy thuốc  Hải Âu",
   "dia_chi": "Ấp 2, thị trấn  Gành Hào, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.0336064",
   "Latitude": "105.4275565"
 },
 {
   "STT": "181",
   "ten_co_so": "Quầy thuốc  Hữu Thịnh",
   "dia_chi": "Ấp Cái Keo, xã An Phúc, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1",
   "Latitude": "105.3"
 },
 {
   "STT": "182",
   "ten_co_so": "Quầy thuốc  Kim Tâm",
   "dia_chi": "Số 34 Ấp Xóm Lớn A, Xã Vĩnh Mỹ A, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.23258549999999",
   "Latitude": "105.5826123"
 },
 {
   "STT": "183",
   "ten_co_so": "Quầy thuốc  Đan Thanh",
   "dia_chi": "Số 212 ấp Thị Trấn B, thị trấn  Hòa Bình, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "184",
   "ten_co_so": "Quầy thuốc  Mai huỳnh",
   "dia_chi": "Số 21, ấp Thị trấn A, thị trấn  Hòa Bình, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "185",
   "ten_co_so": "Nhà thuốc  Ngọc Diệp",
   "dia_chi": "Số 35 Cách Mạng, P.1, thành phố Bạc Liêu",
   "Longtitude": "9.2962419",
   "Latitude": "105.7370994"
 },
 {
   "STT": "186",
   "ten_co_so": "Quầy thuốc  Hoàng Qui",
   "dia_chi": "Số 2/419,  ấp Biển Đông B, xã Vĩnh Trạch Đông, thành phố Bạc Liêu",
   "Longtitude": "9.2661286",
   "Latitude": "105.7938069"
 },
 {
   "STT": "187",
   "ten_co_so": "Quầy thuốc   Hồng Thu",
   "dia_chi": "Số 262/2, ấp Giồng Nhãn, xã Hiệp Thành, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.24115209999999",
   "Latitude": "105.7669687"
 },
 {
   "STT": "188",
   "ten_co_so": "Công ty TNHH Dược Phẩm Gia Nguyễn Bạc Liêu",
   "dia_chi": "Số 26/129 đường Cao Văn Lầu, P. 2, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2475085",
   "Latitude": "105.7308286"
 },
 {
   "STT": "189",
   "ten_co_so": "Quầy thuốc  Diễm My",
   "dia_chi": "Ấp Phước Thọ Tiền, xã Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4130056",
   "Latitude": "105.3950939"
 },
 {
   "STT": "190",
   "ten_co_so": "Quầy thuốc  Tuyết Mai",
   "dia_chi": "Ấp Phước Thành, xã Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4130056",
   "Latitude": "105.3950939"
 },
 {
   "STT": "191",
   "ten_co_so": "Quầy thuốc  Huỳnh Khiêm",
   "dia_chi": "Số 38/10 K. 2, P. Hộ Phòng, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "192",
   "ten_co_so": "Quầy thuốc  Chí Hào",
   "dia_chi": "Số 18, Ấp 19, xã Phong Thạnh, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "15.9128998",
   "Latitude": "79.7399875"
 },
 {
   "STT": "193",
   "ten_co_so": "Nhà thuốc   TTYT Huyện Đông Hải",
   "dia_chi": "Ấp 4, thị trấn  Gành Hào, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.0383639",
   "Latitude": "105.4199115"
 },
 {
   "STT": "194",
   "ten_co_so": "Quầy thuốc  Chí Hải",
   "dia_chi": "Ấp Diêm Điền, xã Điền Hải, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1056847",
   "Latitude": "105.4919293"
 },
 {
   "STT": "195",
   "ten_co_so": "Quầy thuốc  Hải Âu 2",
   "dia_chi": "Ấp A,thị trấn  Gành Hào, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.0336064",
   "Latitude": "105.4275565"
 },
 {
   "STT": "196",
   "ten_co_so": "Quầy thuốc  Số 10",
   "dia_chi": "Số 212, ấp 2, thị trấn  Gành Hào, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.0349627",
   "Latitude": "105.4282431"
 },
 {
   "STT": "197",
   "ten_co_so": "Quầy thuốc  Đông Hải II",
   "dia_chi": "Số 66 Lô G, ấp 3, thị trấn  Gành Hào, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.0349627",
   "Latitude": "105.4282431"
 },
 {
   "STT": "198",
   "ten_co_so": "Quầy thuốc  Minh Anh",
   "dia_chi": "165 đường Nguyễn Trung Trực, Ấp Láng Giài A, thị trấn  Hòa Bình, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.28590629999999",
   "Latitude": "105.6421952"
 },
 {
   "STT": "199",
   "ten_co_so": "Quầy thuốc  Kiều Anh",
   "dia_chi": "Số 67, ấp Thị Trấn B, thị trấn  Hòa Bình, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "200",
   "ten_co_so": "Quầy thuốc  Trung Thu",
   "dia_chi": "Số 80, ấp Vĩnh Mẫu, xã  Vĩnh Hậu, huyện Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.2131068",
   "Latitude": "105.6588485"
 },
 {
   "STT": "201",
   "ten_co_so": "Quầy thuốc  Bích Châu",
   "dia_chi": "Số 96, ấp Thị trấn A, thị trấn  Hòa Bình, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "202",
   "ten_co_so": "Quầy thuốc  Tuyết Giang",
   "dia_chi": "Số 204, ấp Phước Thạnh 1, xã Long Thạnh, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3092534",
   "Latitude": "105.6705801"
 },
 {
   "STT": "203",
   "ten_co_so": "Nhà thuốc  Huyên Thảo",
   "dia_chi": "133A/4 khóm 2, P. 7, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.3036569",
   "Latitude": "105.7197071"
 },
 {
   "STT": "204",
   "ten_co_so": "Quầy thuốc  Thanh Lam ",
   "dia_chi": "5/158B ấp Giồng Giữa B, xã Vĩnh Trạch Đông, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2661286",
   "Latitude": "105.7938069"
 },
 {
   "STT": "205",
   "ten_co_so": "Nhà thuốc  Số 1",
   "dia_chi": "Số 244, Đường Bà Triệu, P.3, thành phố Bạc Liêu",
   "Longtitude": "9.2875976",
   "Latitude": "105.724107"
 },
 {
   "STT": "206",
   "ten_co_so": "Nhà thuốc  Phúc Lộc",
   "dia_chi": "Số 38A đường Bà Triệu, Khóm 1, P. 3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2875976",
   "Latitude": "105.724107"
 },
 {
   "STT": "207",
   "ten_co_so": "Nhà thuốc  Kim Thoa",
   "dia_chi": "Số 20 lô B, đường Hòa Bình, P. 3, thành phố Bạc Liêu,  tỉnh Bạc Liêu",
   "Longtitude": "9.28897939999999",
   "Latitude": "105.7242546"
 },
 {
   "STT": "208",
   "ten_co_so": "Nhà thuốc  Nhân Ái",
   "dia_chi": "Số 25 Phan Đình Phùng, P. 3, thành phố Bạc Liêu,  tỉnh Bạc Liêu",
   "Longtitude": "9.2879364",
   "Latitude": "105.7264888"
 },
 {
   "STT": "209",
   "ten_co_so": "Công ty Cổ Phần Dược Phẩm Bạc Liêu",
   "dia_chi": "Số 99 Hoàng Văn Thụ, P. 3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2865769",
   "Latitude": "105.7258377"
 },
 {
   "STT": "210",
   "ten_co_so": "Quầy thuốc  Trần Danh",
   "dia_chi": "số 9 Nguyễn Thị Minh Khai, thị trấn  Ngan Dừa, huyện huyện  Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.56493959999999",
   "Latitude": "105.4485959"
 },
 {
   "STT": "211",
   "ten_co_so": "Quầy thuốc  Thanh Nhàn",
   "dia_chi": "Ấp Ninh Thạnh Tây, Ninh Thạnh Lợi, huyện  Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.48890879999999",
   "Latitude": "105.3537812"
 },
 {
   "STT": "212",
   "ten_co_so": "Quầy thuốc  Tiến Phúc",
   "dia_chi": "Số 100 Khóm 1, P. Hộ Phòng, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2259938",
   "Latitude": "105.4124112"
 },
 {
   "STT": "213",
   "ten_co_so": "Quầy thuốc  Thu Hiền",
   "dia_chi": "Số 34, Q.Lộ 1A, K. 5, P. Hộ Phòng,thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2346209",
   "Latitude": "105.4438698"
 },
 {
   "STT": "214",
   "ten_co_so": "Quầy thuốc  Gia Bảo",
   "dia_chi": "Số 259 khóm 2, P. 1, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.260136",
   "Latitude": "105.3753129"
 },
 {
   "STT": "215",
   "ten_co_so": "Quầy thuốc  Vũ Gấm",
   "dia_chi": "Số 12 Quốc lộ 1A, Khúc Tréo B, xã Tân Phong, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2114215",
   "Latitude": "105.3680365"
 },
 {
   "STT": "216",
   "ten_co_so": "Quầy thuốc  Huỳnh Hùng",
   "dia_chi": "Số 68 Quốc lộ 1A, khóm 2, P. Hộ Phòng, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.22895",
   "Latitude": "105.4221064"
 },
 {
   "STT": "217",
   "ten_co_so": "Quầy thuốc  Quang Nghiệm",
   "dia_chi": "Số 192, ấp Sóc Đồn, xã Hưng Hội, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3372716",
   "Latitude": "105.7644596"
 },
 {
   "STT": "218",
   "ten_co_so": "Quầy thuốc  Minh Quang",
   "dia_chi": "Số 213 ấp Phước Thạnh 1, xã Long Thạnh, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3092534",
   "Latitude": "105.6705801"
 },
 {
   "STT": "219",
   "ten_co_so": "Quầy thuốc  Thủy Ngọc",
   "dia_chi": "Số 267 ấp Tràm 1, xã Long Thạnh, huyện  Vĩnh Lợi",
   "Longtitude": "9.3281216",
   "Latitude": "105.6545841"
 },
 {
   "STT": "220",
   "ten_co_so": "Quầy thuốc  Ngọc Nhi",
   "dia_chi": "Số 36, Hương Lộ 6, ấp Cù Lao, xã Hưng Hội, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3372716",
   "Latitude": "105.7644596"
 },
 {
   "STT": "221",
   "ten_co_so": "Quầy thuốc  Trung Hiếu",
   "dia_chi": "Ấp Cái Dầy, thị trấn  Châu Hưng, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3432558",
   "Latitude": "105.7149569"
 },
 {
   "STT": "222",
   "ten_co_so": "Quầy thuốc  Phong Phú",
   "dia_chi": "Số 08 ấp Cây Gừa, xã Vĩnh Hậu A, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.1903952",
   "Latitude": "105.6943881"
 },
 {
   "STT": "223",
   "ten_co_so": "Quầy thuốc  Bích Trâm",
   "dia_chi": "Số 22 ấp Ninh Thạnh, xã Ninh Quới A, huyện  Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.51553049999999",
   "Latitude": "105.5122694"
 },
 {
   "STT": "224",
   "ten_co_so": "Quầy thuốc  Số 36- Tuấn",
   "dia_chi": "Số 50 ấp Phước Thành, xã Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4006715",
   "Latitude": "105.4302383"
 },
 {
   "STT": "225",
   "ten_co_so": "Quầy thuốc  Út Chiến",
   "dia_chi": "Ấp 4, xã Phong Thạnh Tây B, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.33897659999999",
   "Latitude": "105.3234646"
 },
 {
   "STT": "226",
   "ten_co_so": "Quầy thuốc  Châu Minh Quân",
   "dia_chi": "Số 61, ấp 2B, xã Phong Thạnh Tây A,  huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.3376512",
   "Latitude": "105.3692249"
 },
 {
   "STT": "227",
   "ten_co_so": "Nhà thuốc  Ngọc Trân",
   "dia_chi": "Số 275, Đường 23 tháng 8, P. 8, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2963195",
   "Latitude": "105.7051607"
 },
 {
   "STT": "228",
   "ten_co_so": "Nhà thuốc  Ngọc Xuân",
   "dia_chi": "Số 127 đường Võ Thị Sáu, P. 8, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2903474",
   "Latitude": "105.7150501"
 },
 {
   "STT": "229",
   "ten_co_so": "Nhà thuốc  Ngọc Phú",
   "dia_chi": "Số 264 Cách Mạng, P.7, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2940027",
   "Latitude": "105.7215663"
 },
 {
   "STT": "230",
   "ten_co_so": "Nhà thuốc  Ngọc Diễn",
   "dia_chi": "Số 126, Đường Cách Mạng, P. 1, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2957663",
   "Latitude": "105.7363325"
 },
 {
   "STT": "231",
   "ten_co_so": "Nhà thuốc  Số 06",
   "dia_chi": "Số 227 đường Trần Huỳnh, P.1, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2968608",
   "Latitude": "105.7276735"
 },
 {
   "STT": "232",
   "ten_co_so": "Nhà Thuốc  Thái Ngọc",
   "dia_chi": "Số 02 Đặng Thùy Trâm, P. 3,thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2841224",
   "Latitude": "105.7207899"
 },
 {
   "STT": "233",
   "ten_co_so": "Nhà Thuốc  Yến Linh",
   "dia_chi": "Số 176 Hòa Bình, P. 3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.28949849999999",
   "Latitude": "105.7242763"
 },
 {
   "STT": "234",
   "ten_co_so": "Nhà Thuốc  Kim Chi",
   "dia_chi": "Số 200 Nguyễn Thị Minh Khai, P. 2, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2877198",
   "Latitude": "105.7347743"
 },
 {
   "STT": "235",
   "ten_co_so": "Quầy thuốc  Phú Sĩ",
   "dia_chi": "Ấp 2B, xã Phong Thạnh Tây A, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.3376512",
   "Latitude": "105.3692249"
 },
 {
   "STT": "236",
   "ten_co_so": "Quầy thuốc  Như Ngọc",
   "dia_chi": "Số 292 ấp Phước 2, xã Vĩnh Phú Tây, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.3678476",
   "Latitude": "105.4419546"
 },
 {
   "STT": "237",
   "ten_co_so": "Quầy thuốc  Vạn An",
   "dia_chi": "Ấp Long Thành, thị trấn  Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4314017",
   "Latitude": "105.4612324"
 },
 {
   "STT": "238",
   "ten_co_so": "Quầy thuốc  Hoàng Tuấn",
   "dia_chi": "Số 303A ấp Long Thành, thị trấn  Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "239",
   "ten_co_so": "Nhà thuốc  Ngọc Diệp",
   "dia_chi": "Quốc lộ 1A, ấp Thị Trấn A1, thị trấn  Hòa Bình, huyện Hòa Bình",
   "Longtitude": "9.2704595",
   "Latitude": "105.5897386"
 },
 {
   "STT": "240",
   "ten_co_so": "Quầy thuốc  Gia An",
   "dia_chi": "Số 245, ấp Láng Giài, thị trấn  Hòa Bình, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.28590629999999",
   "Latitude": "105.6421952"
 },
 {
   "STT": "241",
   "ten_co_so": "Quầy thuốc  Vĩnh Bình",
   "dia_chi": "Số 112 Ấp 18, xã Vĩnh Bình, huyện  Hòa Bình",
   "Longtitude": "9.3444221",
   "Latitude": "105.5608265"
 },
 {
   "STT": "242",
   "ten_co_so": "Quầy thuốc  Ngọc Duyên",
   "dia_chi": "Số 360 ấp Tân Tạo, thị trấn  Châu Hưng, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3388612",
   "Latitude": "105.7292491"
 },
 {
   "STT": "243",
   "ten_co_so": "Quầy thuốc  Giang Văn Bùi",
   "dia_chi": "Số 19 ấp Tam Hưng, xã Vĩnh Hưng, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3871631",
   "Latitude": "105.6119301"
 },
 {
   "STT": "244",
   "ten_co_so": "Nhà thuốc  Chí Tường",
   "dia_chi": "Số 323 đường 23 tháng 8, K. 3, P. 8, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.29653229999999",
   "Latitude": "105.7036138"
 },
 {
   "STT": "245",
   "ten_co_so": "Quầy thuốc  Thiên Thanh",
   "dia_chi": "Nhà không số, ấp Kim Cấu, xã Vĩnh Trạch, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.3090846",
   "Latitude": "105.7938069"
 },
 {
   "STT": "246",
   "ten_co_so": "Quầy thuốc  Triệu Quốc Thái",
   "dia_chi": "Số 161A, ấp Nội Ô, thị trấn  Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "247",
   "ten_co_so": "Quầy thuốc  Nhật Linh",
   "dia_chi": "Nhà không số, ấp Thào Lạng, xã Vĩnh Trạch, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.3090846",
   "Latitude": "105.7938069"
 },
 {
   "STT": "248",
   "ten_co_so": "Quầy thuốc  Thanh Tâm",
   "dia_chi": "Ấp 16 xã Vĩnh Hậu A,huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.26035319999999",
   "Latitude": "105.7521537"
 },
 {
   "STT": "249",
   "ten_co_so": "Quầy thuốc  Mỹ Thu",
   "dia_chi": "Ấp Vĩnh Lạc, xã Vĩnh Thịnh, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.1886028",
   "Latitude": "105.6060661"
 },
 {
   "STT": "250",
   "ten_co_so": "Quầy thuốc  Láng Giài",
   "dia_chi": "Số 169 Ấp Thị Trấn A, thị trấn Hòa Bình, huyện Hòa Bình, Bạc Liêu",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "251",
   "ten_co_so": "Quầy thuốc  Gia Phước ",
   "dia_chi": "Số 256 Quốc lộ 1A, ấp 15, xã Vĩnh Mỹ B, huyện Hòa Bình, Bạc Liêu",
   "Longtitude": "9.2844527",
   "Latitude": "105.6353704"
 },
 {
   "STT": "252",
   "ten_co_so": "Quầy thuốc  Thiên Đức",
   "dia_chi": "Ấp Chùa Phật, thị trấn  Hòa Bình, huyện Hòa Bình,tỉnh Bạc Liêu",
   "Longtitude": "9.2739657",
   "Latitude": "105.6324959"
 },
 {
   "STT": "253",
   "ten_co_so": "Quầy thuốc  Song Ngọc",
   "dia_chi": "Số 4B K. 2, P. Hộ Phòng, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2259938",
   "Latitude": "105.4124112"
 },
 {
   "STT": "254",
   "ten_co_so": "Quầy thuốc   Thịnh Linh",
   "dia_chi": "Số 298, Khóm 2, P. Láng Tròn, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.3036569",
   "Latitude": "105.7197071"
 },
 {
   "STT": "255",
   "ten_co_so": "Quầy thuốc   Gia Hân",
   "dia_chi": "Số 172, ấp Khúc Tréo A, xã Tân Phong,thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.1963166",
   "Latitude": "105.3298112"
 },
 {
   "STT": "256",
   "ten_co_so": "Quầy thuốc   Lê Kim Danh",
   "dia_chi": "Số 168, ấp Khúc Tréo A, xã Tân Phong,thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.1963166",
   "Latitude": "105.3298112"
 },
 {
   "STT": "257",
   "ten_co_so": "Quầy thuốc  Kim Huệ",
   "dia_chi": "Số 88, ấp Nhàn Dân A, xã Tân Phong, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2213584",
   "Latitude": "105.348246"
 },
 {
   "STT": "258",
   "ten_co_so": "Quầy thuốc  Minh Tơ",
   "dia_chi": "Số 58/B Ấp 3, xã Phong Thạnh Đông A, thị xã  Giá Rai,  tỉnh Bạc Liêu",
   "Longtitude": "9.2735574",
   "Latitude": "105.5064087"
 },
 {
   "STT": "259",
   "ten_co_so": "Quầy thuốc  Bảo Toàn",
   "dia_chi": "Số 06, ấp 19, xã Phong Thạnh, thị xã  Giá Rai,  tỉnh Bạc Liêu",
   "Longtitude": "9.3116383",
   "Latitude": "105.393734"
 },
 {
   "STT": "260",
   "ten_co_so": "Quầy thuốc  Ngọc Nhiên",
   "dia_chi": "Khóm 1, P. 1,  thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.238733",
   "Latitude": "105.4592979"
 },
 {
   "STT": "261",
   "ten_co_so": "Quầy thuốc  Thiện Tài",
   "dia_chi": "Ấp Bửu II, xã Long Điền Đông, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1487573",
   "Latitude": "105.5357139"
 },
 {
   "STT": "262",
   "ten_co_so": "Quầy thuốc  Lâm Văn Năm",
   "dia_chi": "Ấp Diêm Điền, xã Điền Hải, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1056847",
   "Latitude": "105.4919293"
 },
 {
   "STT": "263",
   "ten_co_so": "Quầy thuốc  Hoàng Huy",
   "dia_chi": "Ấp Cây Giang, xã Long Điền, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1676975",
   "Latitude": "105.4481035"
 },
 {
   "STT": "264",
   "ten_co_so": "Quầy thuốc  Thảo Quyên",
   "dia_chi": "Ấp Bờ Cảng, xã Điền Hải, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.108406",
   "Latitude": "105.489159"
 },
 {
   "STT": "265",
   "ten_co_so": "Quầy thuốc  Vạn Thọ",
   "dia_chi": "Ấp Nội Ô, thị trấn  Ngan Dừa, huyện  Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "266",
   "ten_co_so": "Quầy thuốc  Khánh Ngân",
   "dia_chi": "Số 122 Quốc lộ 1A, thị trấn  Châu Hưng, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.346774",
   "Latitude": "105.7144571"
 },
 {
   "STT": "267",
   "ten_co_so": "Quầy thuốc  Phương Huy",
   "dia_chi": "số 40A ấp Xẻo Chích, TT Châu Hưng, huyện Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.34961079999999",
   "Latitude": "105.7292491"
 },
 {
   "STT": "268",
   "ten_co_so": "Nhà thuốc  Bích Anh",
   "dia_chi": "Số 3/44 đường Tỉnh lộ 38, P. 5, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2037994",
   "Latitude": "105.7292396"
 },
 {
   "STT": "269",
   "ten_co_so": "Nhà thuốc  Mai Thu",
   "dia_chi": "Số 604/7 Bạch Đằng, P. Nhà Mát, thành phố Bạc Liêu,  tỉnh Bạc Liêu",
   "Longtitude": "9.2269115",
   "Latitude": "105.7362681"
 },
 {
   "STT": "270",
   "ten_co_so": "Nhà thuốc  Kiên Cường",
   "dia_chi": "Số 178/1 đường Trần Huỳnh, P. 7, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2940027",
   "Latitude": "105.7215663"
 },
 {
   "STT": "271",
   "ten_co_so": "Cơ sở bán buôn  Hiền Mai",
   "dia_chi": "Số 10-12 Hai Bà Trưng, P. 3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.28666009999999",
   "Latitude": "105.7249784"
 },
 {
   "STT": "272",
   "ten_co_so": "Cơ sở bán lẻ thuốc dược liệu, thuốc cổ truyền  Vệ Sanh Đường",
   "dia_chi": "Số 59, đường Hoàng Văn Thụ, Khóm 2, P3, thành phốBL, tỉnh Bạc Liêu",
   "Longtitude": "10.8002712",
   "Latitude": "106.6669107"
 },
 {
   "STT": "273",
   "ten_co_so": "Quầy thuốc  Kiều Phí",
   "dia_chi": "Ấp Lung Lá, xã Định Thành A, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1225686",
   "Latitude": "105.2721456"
 },
 {
   "STT": "274",
   "ten_co_so": "Quầy thuốc  Thanh Nhã",
   "dia_chi": "Số 2, ấp Cây Thẻ, xã Định Thành, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1328988",
   "Latitude": "105.2962012"
 },
 {
   "STT": "275",
   "ten_co_so": "Quầy thuốc  Phương Đoan",
   "dia_chi": "Ấp Văn Đức A, xã An Trạch, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1763456",
   "Latitude": "105.3950939"
 },
 {
   "STT": "276",
   "ten_co_so": "Quầy thuốc  Thịnh Cường",
   "dia_chi": "Ấp Cây Thẻ, xã Định Thành, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.2735874",
   "Latitude": "105.3833808"
 },
 {
   "STT": "277",
   "ten_co_so": "Quầy thuốc  Ngọc Điệp",
   "dia_chi": "Số 290 ấp Hòa Thạnh, xã Long Điền, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1676975",
   "Latitude": "105.4481035"
 },
 {
   "STT": "278",
   "ten_co_so": "Quầy thuốc  Bích Thảo",
   "dia_chi": "Số A01 Ấp 3, TTTM, thị trấn  Gành Hào, huyện  Đông Hải, Bạc Liêu",
   "Longtitude": "9.0349627",
   "Latitude": "105.4282431"
 },
 {
   "STT": "279",
   "ten_co_so": "Quầy thuốc  Lộc",
   "dia_chi": "Số 51, Hương Lộ 6,  ấp Cù Lao, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3450296",
   "Latitude": "105.7116464"
 },
 {
   "STT": "280",
   "ten_co_so": "Quầy thuốc  An Khang",
   "dia_chi": "Ấp Năm Căn, xã Hưng Thành, huyện Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3714248",
   "Latitude": "105.8407722"
 },
 {
   "STT": "281",
   "ten_co_so": "Quầy thuốc  Hồng Giang",
   "dia_chi": "Tỉnh lộ 38, ấp Kim Cấu, xã Vĩnh Trạch, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2037994",
   "Latitude": "105.7292396"
 },
 {
   "STT": "282",
   "ten_co_so": "Quầy thuốc    Kim Hưng",
   "dia_chi": "Số 7/238, ấp Biển Tây A, xã Vĩnh Trạch Đông, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2661286",
   "Latitude": "105.7938069"
 },
 {
   "STT": "283",
   "ten_co_so": "Nhà thuốc  Số 4",
   "dia_chi": "Số 250/22B Cao Văn Lầu, K. 3, P.5, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2836754",
   "Latitude": "105.7252548"
 },
 {
   "STT": "284",
   "ten_co_so": "Nhà thuốc  Bệnh Xá Công An",
   "dia_chi": "Số 74 Lê Duẩn, K, 7, P. 1, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.3004123",
   "Latitude": "105.7303371"
 },
 {
   "STT": "285",
   "ten_co_so": "Nhà thuốc  Tuyết Thảo ",
   "dia_chi": "Số 370 Võ Thị Sáu, P.3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2877425",
   "Latitude": "105.7186243"
 },
 {
   "STT": "286",
   "ten_co_so": "Quầy thuốc  Kim Phí",
   "dia_chi": "Ấp Xóm Mới, xã Tân Thạnh, thị xã  Giá Rai",
   "Longtitude": "9.17605599999999",
   "Latitude": "105.2794311"
 },
 {
   "STT": "287",
   "ten_co_so": "Quầy thuốc  Chúc Dư",
   "dia_chi": "Ấp Nhàn Dân B, xã Tân Phong, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.207849",
   "Latitude": "105.356286"
 },
 {
   "STT": "288",
   "ten_co_so": "Quầy thuốc  Hồng Nghi",
   "dia_chi": "Ấp 10B, xã Tân Phong, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.1989667",
   "Latitude": "105.3372637"
 },
 {
   "STT": "289",
   "ten_co_so": "Quầy thuốc  Ngọc Mai",
   "dia_chi": "Quốc lộ 1A, ấp Phước Thạnh 1, xã Long Thạnh, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.293877",
   "Latitude": "105.6744794"
 },
 {
   "STT": "290",
   "ten_co_so": "Quầy thuốc   Bình Nguyên",
   "dia_chi": "Số 278B, ấp Tân Long, Long Thạnh, huyện Vĩnh Lợi, Bạc Liêu",
   "Longtitude": "9.3092534",
   "Latitude": "105.6705801"
 },
 {
   "STT": "291",
   "ten_co_so": "Quầy thuốc  Tùng Bách",
   "dia_chi": "Lô 10 Khu Phố A, Chợ Vĩnh Hưng, ấp Tam Hưng, xã Vĩnh Hưng, huyện  Vĩnh Lợi, Bạc Liêu",
   "Longtitude": "9.389854",
   "Latitude": "105.5994218"
 },
 {
   "STT": "292",
   "ten_co_so": "Quầy thuốc  Tường Oanh",
   "dia_chi": "Số 32 Hương lộ 6, ấp Cả Vĩnh, xã Hưng Hội, huyện  Vĩnh Lợi, Bạc Liêu",
   "Longtitude": "36.778261",
   "Latitude": "-119.4179324"
 },
 {
   "STT": "293",
   "ten_co_so": "Quầy thuốc  Nam Việt",
   "dia_chi": "Ấp Cái Dầy, thị trấn  Châu Hưng, huyện  Vĩnh Lợi, Bạc Liêu",
   "Longtitude": "9.3432558",
   "Latitude": "105.7149569"
 },
 {
   "STT": "294",
   "ten_co_so": "Quầy thuốc  Phúc Khang",
   "dia_chi": "Phía trước bên phải số 259, ấp Cái Dầy,thị trấn  Châu Hưng, huyện  Vĩnh Lợi, Bạc Liêu",
   "Longtitude": "9.3432558",
   "Latitude": "105.7149569"
 },
 {
   "STT": "295",
   "ten_co_so": "Quầy thuốc  Ngô Phát",
   "dia_chi": "Quốc lộ 1A, ấp 15, xã Vĩnh Mỹ B, huyện  Hòa Bình",
   "Longtitude": "9.2798576",
   "Latitude": "105.5597305"
 },
 {
   "STT": "296",
   "ten_co_so": "Quầy thuốc  Hồng Minh",
   "dia_chi": "Số 18/1 QL1A- ấp An Khoa-Vĩnh Mỹ B- Hòa Bình",
   "Longtitude": "9.272243",
   "Latitude": "105.628129"
 },
 {
   "STT": "297",
   "ten_co_so": "Quầy thuốc  Vũ Khôi",
   "dia_chi": "Ấp Chùa Phật, thị trấn  Hòa Bình, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.2739657",
   "Latitude": "105.6324959"
 },
 {
   "STT": "298",
   "ten_co_so": "Quầy thuốc  Ngô Tài",
   "dia_chi": "Số 172 ấp Ba Đình, xã Vĩnh Lộc A, H, Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "21.0337815",
   "Latitude": "105.8140539"
 },
 {
   "STT": "299",
   "ten_co_so": "Quầy thuốc  Hương Thắm",
   "dia_chi": "Số 35, ấp Phước Thành, xã Phước Long, huyện Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4006715",
   "Latitude": "105.4302383"
 },
 {
   "STT": "300",
   "ten_co_so": "Quầy thuốc  Ngọc Trân",
   "dia_chi": "Số 262B ấp Long Thành, thị trấn  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.3092534",
   "Latitude": "105.6705801"
 },
 {
   "STT": "301",
   "ten_co_so": "Bán lẻ thuốc dược liệu và thuốc cổ truyền  Vinh Hưng",
   "dia_chi": "123 đường Hòa Bình, Phường 7, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.28851349999999",
   "Latitude": "105.722841"
 },
 {
   "STT": "302",
   "ten_co_so": "Quầy thuốc  Thanh Tùng",
   "dia_chi": "Lô 28 ấp Phú Tân, xã Ninh Quới, huyện Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.55760499999999",
   "Latitude": "105.5357139"
 },
 {
   "STT": "303",
   "ten_co_so": "Quầy thuốc  Đặng Thúy",
   "dia_chi": "Ấp Long Thành, thị trấn  Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4314017",
   "Latitude": "105.4612324"
 },
 {
   "STT": "304",
   "ten_co_so": "Quầy thuốc  Vũ Luân",
   "dia_chi": "Đường Nguyễn Thị Mười, xã Hưng Phú, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4113246",
   "Latitude": "105.5532993"
 },
 {
   "STT": "305",
   "ten_co_so": "Quầy thuốc  Thảo Nguyên",
   "dia_chi": "Ấp Long Thành, thị trấn  Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4314017",
   "Latitude": "105.4612324"
 },
 {
   "STT": "306",
   "ten_co_so": "Quầy thuốc  Duy Tân",
   "dia_chi": "Ấp 13, xã Phong Thạnh Đông, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.3169",
   "Latitude": "105.4883"
 },
 {
   "STT": "307",
   "ten_co_so": "Quầy thuốc  Kim Lý",
   "dia_chi": "Số 168, Ấp 1, xã Tân Phong, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2213584",
   "Latitude": "105.348246"
 },
 {
   "STT": "308",
   "ten_co_so": "Quầy thuốc  Khải Hoàng",
   "dia_chi": "Số 413, Khóm 1, P. 1, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.260136",
   "Latitude": "105.3753129"
 },
 {
   "STT": "309",
   "ten_co_so": "Quầy thuốc  Long Thịnh",
   "dia_chi": "Số 44 Khóm 1, P. Láng Tròn, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.251623",
   "Latitude": "105.513886"
 },
 {
   "STT": "310",
   "ten_co_so": "Nhà thuốc   Thanh Vũ Medicbaclieu",
   "dia_chi": "Số 02DN, Đường Tránh QL1A, K. 1, P. 7, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.3112002",
   "Latitude": "105.7159208"
 },
 {
   "STT": "311",
   "ten_co_so": "Chi nhánh Công ty cổ phần Dược Hậu Giang tại Bạc Liêu",
   "dia_chi": "số 67, Nguyễn Thị Định, khóm 10, phường 1, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.3076594",
   "Latitude": "105.7328168"
 },
 {
   "STT": "312",
   "ten_co_so": "Nhà thuốc  Ngọc Nhi",
   "dia_chi": "157, Nguyễn Thị Minh Khai, khóm 2, phường 5, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2833256",
   "Latitude": "105.7278596"
 },
 {
   "STT": "313",
   "ten_co_so": "Quầy thuốc  Thành Đại",
   "dia_chi": "ấp Bửu 2, xã Long Điền Đông, huyện Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1487573",
   "Latitude": "105.5357139"
 },
 {
   "STT": "314",
   "ten_co_so": "Quầy thuốc  Ngọc Lượng",
   "dia_chi": "số 88, ấp Thuận Điền, xã Long Điền Tây, huyện Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "10.1908477",
   "Latitude": "106.4111425"
 },
 {
   "STT": "315",
   "ten_co_so": "Quầy thuốc  Thiện Phước",
   "dia_chi": "số 46, ấp Diêm Điền, xã Điền Hải, huyện Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.10787869999999",
   "Latitude": "105.488828"
 },
 {
   "STT": "316",
   "ten_co_so": "Quầy thuốc  Ngọc Hân",
   "dia_chi": "số 39, ấp Thạnh II, xã long Điền, huyện Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1676975",
   "Latitude": "105.4481035"
 },
 {
   "STT": "317",
   "ten_co_so": "Quầy thuốc  Mỹ Ảnh",
   "dia_chi": "ấp Ba mến, xã An Trạch A, huyện Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1751556",
   "Latitude": "105.3773249"
 },
 {
   "STT": "318",
   "ten_co_so": "Cơ sở bán buôn thuốc thành phẩm  Thái Nhựt",
   "dia_chi": "Số 07 Lầu 1, đường Tôn Đức Thắng, P. 1, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2984779",
   "Latitude": "105.7356342"
 },
 {
   "STT": "319",
   "ten_co_so": "Công ty TNHH Dược Phẩm Gia Nguyễn Bạc Liêu",
   "dia_chi": "Số 26/129 đường Cao Văn Lầu, P. 2, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2475085",
   "Latitude": "105.7308286"
 },
 {
   "STT": "320",
   "ten_co_so": "Công ty Cổ Phần Dược Phẩm Bạc Liêu",
   "dia_chi": "Số 99 Hoàng Văn Thụ, P. 3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2865769",
   "Latitude": "105.7258377"
 },
 {
   "STT": "321",
   "ten_co_so": "Hiền Mai",
   "dia_chi": "Số 10-12 Hai Bà Trưng, P. 3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.28666009999999",
   "Latitude": "105.7249784"
 },
 {
   "STT": "322",
   "ten_co_so": "Chi nhánh Công ty cổ phần Dược Hậu Giang tại Bạc Liêu",
   "dia_chi": "số 67, Nguyễn Thị Định, khóm 10, phường 1, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.3076594",
   "Latitude": "105.7328168"
 }
];