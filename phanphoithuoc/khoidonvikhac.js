var datakhoidonvikhac = [
 {
   "STT": "1",
   "Name": "Trung tâm Truyền thông giáo dục sức khỏe Bạc Liêu",
   "area": "Tỉnh",
   "address": "Số 154 Hòa Bình, Phường 3, thành phố Bạc Liêu",
   "Longtitude": "9.2894337",
   "Latitude": "105.7253536"
 },
 {
   "STT": "2",
   "Name": "Trung tâm Phòng, chống bệnh xã hội Bạc Liêu",
   "area": "Tỉnh",
   "address": "Số 76 Lê Duẩn, Phường 1, thành phố Bạc Liêu",
   "Longtitude": "9.2943158",
   "Latitude": "105.7286453"
 },
 {
   "STT": "3",
   "Name": "Trung tâm Kiểm nghiệm Thuốc, Mỹ phẩm và Thực phẩm Bạc Liêu",
   "area": "Tỉnh",
   "address": "Số 67 Điện Biên Phủ, Phường 3, thành phố Bạc Liêu",
   "Longtitude": "9.28585949999999",
   "Latitude": "105.7264282"
 },
 {
   "STT": "4",
   "Name": "Trung tâm Y tế dự phòng Bạc Liêu",
   "area": "Tỉnh",
   "address": "Số 199 Hoàng Diệu, Phường 1, thành phố Bạc Liêu",
   "Longtitude": "9.2889743",
   "Latitude": "105.7300176"
 },
 {
   "STT": "5",
   "Name": "Trung tâm Phòng, chống HIV/AIDS Bạc Liêu",
   "area": "Tỉnh",
   "address": "Tỉnh lộ 38, ấp Vĩnh An, xã Vĩnh Trạch, TP. Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.3090846",
   "Latitude": "105.7938069"
 },
 {
   "STT": "6",
   "Name": "Trung tâm Chăm sóc sức khỏe sinh sản Bạc Liêu",
   "area": "Tỉnh",
   "address": "2/9, Hòa Bình, Phường 1, TP. Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2871141",
   "Latitude": "105.7200495"
 },
 {
   "STT": "8",
   "Name": "Trung tâm Giám định y khoa Bạc Liêu",
   "area": "Tỉnh",
   "address": "Số 30, Đinh Bộ Lĩnh, Phường 3, TP. Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.28225039999999",
   "Latitude": "105.7208745"
 },
 {
   "STT": "9",
   "Name": "Chi cục Dân số - Kế hoạch hóa gia đình Bạc Liêu",
   "area": "Tỉnh",
   "address": "Số 76, Lê Duẩn, Phường 1, TP. Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2943158",
   "Latitude": "105.7286453"
 },
 {
   "STT": "10",
   "Name": "Chi cục An toàn vệ sinh thực phẩm",
   "area": "Tỉnh",
   "address": "Số 32, Nguyễn Du, Phường 5, TP. Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2840063",
   "Latitude": "105.7273596"
 }
];