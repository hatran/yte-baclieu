var datanhathuoc = [
 {
   "STT": "1",
   "Name": "Nhà thuốc  Tuyết Thảo 3",
   "address": "số 33 đường Nguyễn Huệ, phường 3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2838539",
   "Latitude": "105.7208581"
 },
 {
   "STT": "2",
   "Name": "Nhà thuốc  Khai Minh 2",
   "address": "số 42 đường Nguyễn Huệ, phường 3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2838539",
   "Latitude": "105.7208581"
 },
 {
   "STT": "3",
   "Name": "Nhà thuốc  Quốc Khánh",
   "address": "số 432 đường Võ Thị Sáu, phường 3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2882333",
   "Latitude": "105.7181391"
 },
 {
   "STT": "4",
   "Name": "Nhà thuốc  Vân Trang",
   "address": "số 195 đường Bà Triệu, phường 3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2875976",
   "Latitude": "105.724107"
 },
 {
   "STT": "5",
   "Name": "Nhà thuốc  Hồng Hà",
   "address": "số D 04/96 đường 23 tháng 8, phường 8, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2968652",
   "Latitude": "105.7181458"
 },
 {
   "STT": "6",
   "Name": "Nhà thuốc  Anh Lực",
   "address": "Số 98 Tỉnh lộ 38, khóm 7, Phường 5, thành phố Bạc Liêu, Tỉnh Bạc Liêu",
   "Longtitude": "9.2061818",
   "Latitude": "105.7348179"
 },
 {
   "STT": "7",
   "Name": "Nhà thuốc  Tuyết Anh",
   "address": "Nhà không số(thửa đất 882, tờ bản đồ số 3), Liên tỉnh lộ 38, Phường 5, thành phố Bạc Liêu, Tỉnh Bạc Liêu",
   "Longtitude": "9.2037994",
   "Latitude": "105.7292396"
 },
 {
   "STT": "8",
   "Name": "Nhà thuốc  Ánh Dương",
   "address": "Số 51A, đường Lý Thường Kiệt, khóm 4, Phường 3,thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2889538",
   "Latitude": "105.7251585"
 },
 {
   "STT": "9",
   "Name": "Nhà thuốc  An Kỳ",
   "address": "Số 429, đường 23/8, Phường 8, thành phố Bạc Liêu",
   "Longtitude": "9.2967004",
   "Latitude": "105.7026486"
 },
 {
   "STT": "10",
   "Name": "Nhà thuốc  Minh Hiếu",
   "address": "Số 346 Võ Thị Sáu, P.3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2882578",
   "Latitude": "105.7180262"
 },
 {
   "STT": "11",
   "Name": "Nhà thuốc  Trung Nghĩa",
   "address": "Số 27, đường Ninh Bình, Phường 2, thành phố Bạc Liêu, Tỉnh Bạc Liêu",
   "Longtitude": "9.2738687",
   "Latitude": "105.7409852"
 },
 {
   "STT": "12",
   "Name": "Nhà thuốc  Ngọc Yến",
   "address": "Số 37, đường Hà Huy Tập, Phường 3, thành phố Bạc Liêu, Tỉnh Bạc Liêu",
   "Longtitude": "9.2868248",
   "Latitude": "105.7245575"
 },
 {
   "STT": "13",
   "Name": "Nhà thuốc  Hòa Thành",
   "address": "Số 187A, đường Nguyễn Thị Minh Khai, Phường 5, thành phố Bạc Liêu, Tỉnh Bạc Liêu",
   "Longtitude": "9.2838615",
   "Latitude": "105.7285307"
 },
 {
   "STT": "14",
   "Name": "Nhà thuốc  Quốc Lợi",
   "address": "Số 179, đường Bà Triệu, Phường 3, thành phố Bạc Liêu, Tỉnh Bạc Liêu",
   "Longtitude": "9.2875976",
   "Latitude": "105.724107"
 },
 {
   "STT": "15",
   "Name": "Nhà thuốc  Xuân Hoàng",
   "address": "Số 89/53, đường Cách Mạng, Phường 1, thành phố Bạc Liêu, Tỉnh Bạc Liêu",
   "Longtitude": "9.3004123",
   "Latitude": "105.7303371"
 },
 {
   "STT": "16",
   "Name": "Quầy thuốc  Thiên Kim",
   "address": "Số 20 ấp Long Hậu, TT Phước Long, huyện Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4377778",
   "Latitude": "105.4630556"
 },
 {
   "STT": "17",
   "Name": "Quầy thuốc  Thanh Nhàn",
   "address": "Số 291, ấp Long Thành,TT Phước Long, huyện Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.36576509999999",
   "Latitude": "105.488828"
 },
 {
   "STT": "18",
   "Name": "Quầy thuốc  Phước Tài",
   "address": "Số 103A, ấp Nội Ô, thị trấn  Phước Long, huyện Phước Long",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "20",
   "Name": "Quầy thuốc  Quốc Dũng",
   "address": "Ấp Vĩnh Hòa, xã Vĩnh Thanh, huyện Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.3536316",
   "Latitude": "105.522889"
 },
 {
   "STT": "21",
   "Name": "Quầy thuốc  Thanh Vân",
   "address": "196B, ấp Long Thành, TT Phước Long, huyện Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "22",
   "Name": "Quầy thuốc  Minh Hiếu",
   "address": "Số 121, ấp Sóc Đồn, xã Hưng Hội, huyện Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3372716",
   "Latitude": "105.7644596"
 },
 {
   "STT": "23",
   "Name": "Quầy thuốc  Mỹ Linh",
   "address": "Số 78 Ấp Thị Trấn A, thị trấn  Hòa Bình, huyện  Hòa Bình",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "24",
   "Name": "Quầy thuốc  Tuấn Khôi",
   "address": "Ấp 16B, xã Phong Tân, Thị xã Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.3017",
   "Latitude": "105.4403"
 },
 {
   "STT": "25",
   "Name": "Quầy thuốc  Thảo My",
   "address": "Số 222, K. 4, P.1, Thị xã Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.260136",
   "Latitude": "105.3753129"
 },
 {
   "STT": "26",
   "Name": "Quầy thuốc  Diễm Thy",
   "address": "Số 536, K.5, P,1, thị xã Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.260136",
   "Latitude": "105.3753129"
 },
 {
   "STT": "27",
   "Name": "Quầy thuốc   Kim Tuyến",
   "address": "Số 07, ấp Khúc Tréo A, xã Tân Phong, Thị xã Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.1986972",
   "Latitude": "105.3301616"
 },
 {
   "STT": "28",
   "Name": "Quầy thuốc  Ngọc Bo",
   "address": "Số 404, ấp Khúc Tréo B, xã Tân Phong, thị xã  Giá Rai,tỉnh Bạc Liêu",
   "Longtitude": "9.1963166",
   "Latitude": "105.3298112"
 },
 {
   "STT": "29",
   "Name": "Quầy thuốc  Thanh Hải",
   "address": "Số 411 Q.lộ 1A, ấp khúc Tréo A, xã Tân Phong, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.1989667",
   "Latitude": "105.3372637"
 },
 {
   "STT": "30",
   "Name": "Thuốc đông y  Tế Nhơn An 222",
   "address": "Ấp Nhàn Dân A, xã Tân Phong, TX Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.18879999999999",
   "Latitude": "105.308593"
 },
 {
   "STT": "31",
   "Name": "Thuốc đông y  Trường Ngươn Đường",
   "address": "Số 398, đường 30/4, khóm 2, phường Hộ Phòng, TX Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "32",
   "Name": "Quầy thuốc  Hải Yến ",
   "address": "Số 2, ấp Khúc Tréo B, xã Tân Phong, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.1963166",
   "Latitude": "105.3298112"
 },
 {
   "STT": "33",
   "Name": "Quầy thuốc  Thanh Hoa",
   "address": "Kios 05, đường Huỳnh Hoàng Hùng, K. 2, P. Hộ Phòng, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "34",
   "Name": "Quầy thuốc  Quang thái",
   "address": "Số 144, khóm 1, P. Hộ Phòng, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "35",
   "Name": "Quầy thuốc  Ánh Hồng",
   "address": "Số 273E Phan Thanh Giản, K. 5, thị xã  Giá Rai",
   "Longtitude": "9.260136",
   "Latitude": "105.3753129"
 },
 {
   "STT": "36",
   "Name": "Quầy thuốc  Ngọc Mai",
   "address": "Số 2, Khóm 2, P. Hộ Phòng, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2259938",
   "Latitude": "105.4124112"
 },
 {
   "STT": "37",
   "Name": "Quầy thuốc  Thanh Đạm",
   "address": "Số 450 ấp 2, xã Phong Thạnh Đông A, thị xã  Giá Rai",
   "Longtitude": "9.2735574",
   "Latitude": "105.5064087"
 },
 {
   "STT": "38",
   "Name": "Quầy thuốc  Minh Nguyên",
   "address": "Số 34, K. 2, P. Hộ Phòng, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "39",
   "Name": "Quầy thuốc  Quốc Dũng",
   "address": "666, âp An Khoa, xã Vĩnh Mỹ B, huyện Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.28581889999999",
   "Latitude": "105.5974766"
 },
 {
   "STT": "40",
   "Name": "Quầy thuốc  Cao Thành ",
   "address": "Ấp Vĩnh Lạc, xã Vĩnh Thịnh, huyện Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.1886028",
   "Latitude": "105.6060661"
 },
 {
   "STT": "41",
   "Name": "Quầy thuốc  Mộng Nghi",
   "address": "Ấp 15, xã Vĩnh Mỹ B, huyện Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.29349519999999",
   "Latitude": "105.5648804"
 },
 {
   "STT": "42",
   "Name": "Quầy thuốc  Quốc Minh",
   "address": "Số 36, ấp Xóm Lớn B, xã Vĩnh Mỹ A, huyện Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.23258549999999",
   "Latitude": "105.5826123"
 },
 {
   "STT": "43",
   "Name": "Quầy thuốc  Ngọc Thảo ",
   "address": "Ấp Bờ Cảng, xã Điền Hải, huyện Đông Hải,tỉnh Bạc Liêu",
   "Longtitude": "9.108406",
   "Latitude": "105.489159"
 },
 {
   "STT": "44",
   "Name": "Quầy thuốc  Hồng Thảnh",
   "address": "Ấp Diêm Điền, Xã Điền Hải, huyện Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1056847",
   "Latitude": "105.4919293"
 },
 {
   "STT": "45",
   "Name": "Quầy thuốc  Cầm Nên",
   "address": "Ấp Diêm Điền, Xã Điền Hải, huyện Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1056847",
   "Latitude": "105.4919293"
 },
 {
   "STT": "46",
   "Name": "Quầy thuốc  Thu Ngân",
   "address": "Số 112, ấp Cây Thẻ, xã Định Thành, huyện Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1323056",
   "Latitude": "105.2955575"
 },
 {
   "STT": "47",
   "Name": "Quầy thuốc  Tư Kỳ",
   "address": "Ấp Văn Đức A, xã An Trạch,huyện Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1763456",
   "Latitude": "105.3950939"
 },
 {
   "STT": "48",
   "Name": "Quầy thuốc  Nhã Uyên",
   "address": "Số 332 ấp Mỹ Điền, xã Long Điền Đông A, huyện Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.21670649999999",
   "Latitude": "105.5361154"
 },
 {
   "STT": "49",
   "Name": "Quầy thuốc  Hoàng Tỷ",
   "address": "Số 339 Ấp 2, thị trấn  Gành Hào, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.0349627",
   "Latitude": "105.4282431"
 },
 {
   "STT": "50",
   "Name": "Quầy thuốc  Lâm Văn Năm",
   "address": "Số 41B ấp Diêm Điền, xã Điền Hải, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.10787869999999",
   "Latitude": "105.488828"
 },
 {
   "STT": "51",
   "Name": "Quầy thuốc  Kim Thúy",
   "address": "Số 358, ấp Cái Tràm A1, xã Long Thạnh, huyện Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.2861259",
   "Latitude": "105.6822968"
 },
 {
   "STT": "52",
   "Name": "Quầy thuốc  Trúc Xuân",
   "address": "Số 194, ấp Trà Ban 2, xã Châu Hưng A, huyện Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.37675679999999",
   "Latitude": "105.7233814"
 },
 {
   "STT": "53",
   "Name": "Quầy thuốc  Thúy Nhi",
   "address": "ấp Xẻo Chích, thị trấn Châu Hưng, huyện Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3333333",
   "Latitude": "105.7166667"
 },
 {
   "STT": "54",
   "Name": "Quầy thuốc  Toàn Tâm",
   "address": " ấp Trà Ban I, xã Châu Hưng A, huyện Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.37768829999999",
   "Latitude": "105.7135109"
 },
 {
   "STT": "55",
   "Name": "Quầy thuốc  Cẩm Tấn",
   "address": "Quốc lộ 1A, ấp Cái Dầy, thị trấn Châu Hưng, huyện Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3432558",
   "Latitude": "105.7149569"
 },
 {
   "STT": "56",
   "Name": "Quầy thuốc  Gia Huy",
   "address": "Số 90, ấp Tam Hưng, xã Vĩnh Hưng, huyện Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3871631",
   "Latitude": "105.6119301"
 },
 {
   "STT": "57",
   "Name": "Quầy thuốc  Út Huệ",
   "address": "Số 134, ấp Cái Dầy, thị trấn Châu Hưng, huyện Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3432558",
   "Latitude": "105.7149569"
 },
 {
   "STT": "58",
   "Name": "Quầy thuốc  Hồng Anh",
   "address": "Số 254, ấp Phước Thạnh 1, xã Long Thạnh, huyện Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3092534",
   "Latitude": "105.6705801"
 },
 {
   "STT": "59",
   "Name": "Quầy thuốc  Tuấn Đạt",
   "address": "Số 037, Khu IB, ấp Nội Ô, thị trấn Ngan Dừa, huyện Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "60",
   "Name": "Quầy thuốc  Minh Hiệp 1",
   "address": "Số 240 khu vực II, ấp Nội Ô, thị trấn Ngan Dừa, huyện Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "61",
   "Name": "Quầy thuốc  Kim Dững",
   "address": "ấp Nội Ô, thị trấn  Ngan Dừa, huyện  Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "62",
   "Name": "Quầy thuốc  Văn Chiến",
   "address": "Số 334, ấp Vĩnh Hòa, xã Vĩnh Thanh, Phước Long, Bạc Liêu",
   "Longtitude": "9.36576509999999",
   "Latitude": "105.488828"
 },
 {
   "STT": "63",
   "Name": "Quầy thuốc  Phước Vũ",
   "address": "Số 73, ấp Phước Thành, xã Phước Long, huyện  Phước Long, Bạc Liêu",
   "Longtitude": "9.4130056",
   "Latitude": "105.3950939"
 },
 {
   "STT": "64",
   "Name": "Quầy thuốc  Dương Chiêu",
   "address": "Số 118/B, ấp Nội Ô, thị trấn  Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "65",
   "Name": "Quầy thuốc  Thanh Trung",
   "address": "Số 328/B, ấp Nội Ô, thị trấn  Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "66",
   "Name": "Quầy thuốc  Ngọc Nhung",
   "address": "Số 1/114, ấp Vĩnh An, xã Vĩnh Trạch, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.37853099999999",
   "Latitude": "105.3946"
 },
 {
   "STT": "67",
   "Name": "Nhà thuốc  Bà Triệu",
   "address": "31 Bà Triệu - P3-thành phố.  Bạc Liêu",
   "Longtitude": "9.2877662",
   "Latitude": "105.7242412"
 },
 {
   "STT": "68",
   "Name": "Nhà thuốc  Thế Cường",
   "address": "Số 125 đường Cách Mạng, K. 10, P. 1, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2940027",
   "Latitude": "105.7215663"
 },
 {
   "STT": "69",
   "Name": "Nhà thuốc   Thanh Long",
   "address": "171, Võ Thị Sáu, P.7, thành phố Bạc Liêu",
   "Longtitude": "9.2912161",
   "Latitude": "105.7139066"
 },
 {
   "STT": "70",
   "Name": "Nhà thuốc   Hiền Khanh",
   "address": "Số 29 đường Cách Mạng, K. 10, P. 1, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2957663",
   "Latitude": "105.7363325"
 },
 {
   "STT": "71",
   "Name": "Nhà thuốc  Khánh Trung",
   "address": "101-Trần Huỳnh-Phường 7-thành phố Bạc Liêu",
   "Longtitude": "9.2936646",
   "Latitude": "105.7202339"
 },
 {
   "STT": "72",
   "Name": "Quầy thuốc  Minh Tuấn",
   "address": "Ấp Vĩnh Mẫu, xã Vĩnh Hậu, huyện Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.2131068",
   "Latitude": "105.6588485"
 },
 {
   "STT": "73",
   "Name": "Quầy thuốc  Cẩm Tú",
   "address": "Số 688, ấp B1, thị trấn  Hòa Bình, huyện Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "74",
   "Name": "Quầy thuốc  Thành Đạt",
   "address": "Số 179 Quốc lộ 1A, ấp thị trấn A, thị trấn  Hòa Bình, huyện Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "75",
   "Name": "Quầy thuốc  Phương Vy",
   "address": "Ấp Long Thành, thị trấn  Phước Long, huyện Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4314017",
   "Latitude": "105.4612324"
 },
 {
   "STT": "76",
   "Name": "Quầy thuốc  Hồng Ảnh",
   "address": "Ấp Tường Tư, xã Hưng Phú, huyện Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.3858316",
   "Latitude": "105.518982"
 },
 {
   "STT": "77",
   "Name": "Quầy thuốc  Xuân Lộc",
   "address": "Ấp Nội Ô, thị trấn Phước Long, Phước Long, Bạc Liêu",
   "Longtitude": "9.4478981",
   "Latitude": "105.4616474"
 },
 {
   "STT": "78",
   "Name": "Quầy thuốc  Lynh Uyên",
   "address": "Số 511, Phan Thanh Giản, Ấp 5,  thị trấn  Giá Rai, huyện Giá Rai",
   "Longtitude": "9.2388149",
   "Latitude": "105.4586323"
 },
 {
   "STT": "79",
   "Name": "Quầy thuốc  Tuấn Tú",
   "address": "464 ấp Nhàn Dân A, xã Tân Phong, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2213584",
   "Latitude": "105.348246"
 },
 {
   "STT": "80",
   "Name": "Nhà thuốc Bệnh Viện  Bệnh viện",
   "address": "K.1, P. 1, thị xã  Giá Rai, Bạc Liêu",
   "Longtitude": "9.260136",
   "Latitude": "105.3753129"
 },
 {
   "STT": "81",
   "Name": "Nhà Thuốc  Huỳnh Tuấn",
   "address": "Số 110, khóm 2, P. Hộ Phòng, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "82",
   "Name": "Quầy thuốc   Liễu",
   "address": "Kios 37-39, đường Nguyễn Quốc Hương, K.2, P. Hộ Phòng, thị xã  Giá Rai, Bạc Liêu",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "83",
   "Name": "Quầy thuốc  Minh Quyền",
   "address": "Số 656, Khóm 2, P. Láng Tròn, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2735574",
   "Latitude": "105.5064087"
 },
 {
   "STT": "84",
   "Name": "Quầy thuốc  Tố Liên",
   "address": "Số 453, K. 2, P. Láng Tròn, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2735574",
   "Latitude": "105.5064087"
 },
 {
   "STT": "85",
   "Name": "Cơ sở bán buôn thuốc thành phẩm  Thái Nhựt",
   "address": "Số 07 Lầu 1, đường Tôn Đức Thắng, P. 1, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2984779",
   "Latitude": "105.7356342"
 },
 {
   "STT": "86",
   "Name": "Quầy thuốc  Thành Huệ",
   "address": "Số 330 Ấp Nội ô, thị trấn  Ngan Dừa, huyện  Hồng Dân",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "87",
   "Name": "Quầy thuốc  Nguyễn Thanh Tùng",
   "address": "111, ấp Ninh Thạnh, xã Ninh Quới A, Hồng Dân, Bạc Liêu",
   "Longtitude": "9.5668968",
   "Latitude": "105.4529572"
 },
 {
   "STT": "88",
   "Name": "Quầy thuốc  Thanh Bình II",
   "address": "Ki ốt B ấp Nội Ô, thị trấn  Ngan Dừa, huyện  Hồng Dân",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "89",
   "Name": "Nhà thuốc  Hiếu Dân",
   "address": "30,  Hà Huy Tập,  P.3 thành phố Bạc Liêu",
   "Longtitude": "9.2870607",
   "Latitude": "105.7244207"
 },
 {
   "STT": "90",
   "Name": "Nhà thuốc  Hữu Tâm",
   "address": "Số 07, Đường Hà Huy Tập,  Phường 3, thành phố Bạc Liêu",
   "Longtitude": "9.2868248",
   "Latitude": "105.7245575"
 },
 {
   "STT": "91",
   "Name": "Nhà thuốc  Anh Thư",
   "address": "Số 208 Võ Thị Sáu, P.3, thành phố Bạc Liêu",
   "Longtitude": "9.29073629999999",
   "Latitude": "105.7145578"
 },
 {
   "STT": "92",
   "Name": "Quầy thuốc  Minh Đức",
   "address": "Số 27, ấp Nhà Việc, xã Châu Thới, Vĩnh Lợi, Bạc Liêu",
   "Longtitude": "9.358423",
   "Latitude": "105.652983"
 },
 {
   "STT": "93",
   "Name": "Quầy thuốc  Việt Hằng",
   "address": "Số 131, ấp Cái Dầy, TT Châu Hưng, Vĩnh Lợi, Bạc Liêu",
   "Longtitude": "9.3432558",
   "Latitude": "105.7149569"
 },
 {
   "STT": "94",
   "Name": "Quầy thuốc  Út Huệ 2",
   "address": "Số 273, ấp Cái Dầy, thị trấn  Châu Hưng, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3432558",
   "Latitude": "105.7149569"
 },
 {
   "STT": "95",
   "Name": "Quầy thuốc  Dương Quân",
   "address": "Số 40, ấp Xẻo Chích, thị trấn  Châu Hưng, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3388612",
   "Latitude": "105.7292491"
 },
 {
   "STT": "96",
   "Name": "Quầy thuốc  Minh Minh",
   "address": "Ấp Gò Cát, xã Điền Hải, Đông Hải, Bạc Liêu",
   "Longtitude": "9.1058995",
   "Latitude": "105.491815"
 },
 {
   "STT": "97",
   "Name": "Quầy thuốc  Tấn Đạt",
   "address": "Số 142, ấp Bờ Cảng, xã Điền Hải, Bạc Liêu",
   "Longtitude": "9.10787869999999",
   "Latitude": "105.488828"
 },
 {
   "STT": "98",
   "Name": "Quầy thuốc  Đạt Sang II",
   "address": "309 ấp 4 thị trấn Gành Hào, huyện Đông Hải",
   "Longtitude": "9.0383639",
   "Latitude": "105.4199115"
 },
 {
   "STT": "99",
   "Name": "Quầy thuốc  Long Điền",
   "address": "ấp Cây Giang, xã Long Điền, Đông Hải, Bạc Liêu",
   "Longtitude": "9.1676975",
   "Latitude": "105.4481035"
 },
 {
   "STT": "100",
   "Name": "Quầy thuốc  Tuấn Phong",
   "address": "Số 74, ấp 3, thị trấn  Gành Hào, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.0349627",
   "Latitude": "105.4282431"
 },
 {
   "STT": "101",
   "Name": "Quầy thuốc  Thanh Nga",
   "address": "Số 15, ấp Thị Trấn A, thị trấn  Hòa Bình, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "102",
   "Name": "Quầy thuốc  Ninh Đẹp",
   "address": "Số 278B, ấp Nội Ô, thị trấn  Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "103",
   "Name": "Quầy thuốc  Số 20",
   "address": "Số 346B, ấp Nội Ô, thị trấn  Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "104",
   "Name": "Quầy thuốc  Hiếu Lộc",
   "address": "Số 389, ấp Nhàn Dân A, xã Tân Phong, huyện Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2213584",
   "Latitude": "105.348246"
 },
 {
   "STT": "105",
   "Name": "Quầy thuốc  Út An",
   "address": "Số 120 Ấp Nội Ô, thị trấn  Ngan Dừa, huyện  Hồng Dân",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "106",
   "Name": "Quầy thuốc  Kim Kha",
   "address": "Số 2/444, ấp Biển Đông B, xã Vĩnh Trạch Đông, thành phốBL, Bạc Liêu",
   "Longtitude": "9.2661286",
   "Latitude": "105.7938069"
 },
 {
   "STT": "107",
   "Name": "Nhà thuốc  Tường Nhi",
   "address": "Số 309 Cao Văn Lầu, P.5, thành phố Bạc Liêu",
   "Longtitude": "9.2745549",
   "Latitude": "105.7296603"
 },
 {
   "STT": "108",
   "Name": "Nhà thuốc   Thanh Vũ",
   "address": "26A Bà Triệu, P3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2886717",
   "Latitude": "105.7262786"
 },
 {
   "STT": "109",
   "Name": "Nhà thuốc  Linh Đan",
   "address": "Số 7/3, Đường Nguyễn Tất Thành, P.7, thành phố Bạc Liêu",
   "Longtitude": "9.2940027",
   "Latitude": "105.7215663"
 },
 {
   "STT": "110",
   "Name": "Quầy thuốc  Ngọc Nhi",
   "address": "Số 36 ấp Cù Lao, xã Hưng Hội, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3028015",
   "Latitude": "105.744701"
 },
 {
   "STT": "111",
   "Name": "Quầy thuốc   Khánh Bình",
   "address": "Ấp Chùa Phật, thị trấn Hòa Bình, huyện Hòa Bình, Bạc Liêu",
   "Longtitude": "9.2739657",
   "Latitude": "105.6324959"
 },
 {
   "STT": "112",
   "Name": "Quầy thuốc  Thống Thắng ",
   "address": "142 ấp Vĩnh Lạc, xã Vĩnh Thịnh, Hòa Bình, Bạc Liêu",
   "Longtitude": "9.1886028",
   "Latitude": "105.6060661"
 },
 {
   "STT": "113",
   "Name": "Quầy thuốc  Tuấn Đạt",
   "address": "ấp Xóm Lớn B, xã Vĩnh Mỹ A, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.23258549999999",
   "Latitude": "105.5826123"
 },
 {
   "STT": "114",
   "Name": "Quầy thuốc   Huỳnh Châu Thiệt",
   "address": "Số 253, K. 1, P. 1, thị xã Giá Rai,tỉnh Bạc Liêu",
   "Longtitude": "9.260136",
   "Latitude": "105.3753129"
 },
 {
   "STT": "115",
   "Name": "Quầy thuốc  Phúc Hậu",
   "address": "số 20 ấp Xóm Mới, xã Tân Thạnh, TX Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2090313",
   "Latitude": "105.2604409"
 },
 {
   "STT": "118",
   "Name": "Quầy thuốc  Diệu Hùng",
   "address": "Số 125, ấp Phước Thành, xã Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.3678476",
   "Latitude": "105.4419546"
 },
 {
   "STT": "119",
   "Name": "Quầy thuốc  Thoại Trang",
   "address": "Số 96B, ấp Nội Ô, thị trấn  Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "120",
   "Name": "Quầy thuốc  Số 11",
   "address": "Số 01 ấp Ninh Thạnh, Ninh Quới A, huyện  Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.51553049999999",
   "Latitude": "105.5122694"
 },
 {
   "STT": "121",
   "Name": "Quầy thuốc  Thiện Phước ",
   "address": "Số 46, ấp Diêm Điền, xã Điền Hải, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.10787869999999",
   "Latitude": "105.488828"
 },
 {
   "STT": "122",
   "Name": "Nhà thuốc  Khai Minh",
   "address": "23 Hà Huy Tập, P.3,thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.28713769999999",
   "Latitude": "105.7244861"
 },
 {
   "STT": "123",
   "Name": "Nhà thuốc  Thuận An",
   "address": "Số 360 Võ Thị Sáu, P.3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2879718",
   "Latitude": "105.718448"
 },
 {
   "STT": "124",
   "Name": "Nhà thuốc  Duy Luân",
   "address": "Số 270A/5 Cao Văn Lầu, Khóm Đầu Lộ A, P. Nhà Mát, thành phố Bạc Liêu",
   "Longtitude": "9.2836381",
   "Latitude": "105.7254046"
 },
 {
   "STT": "125",
   "Name": "Nhà thuốc  Thảo Minh",
   "address": "Số 2/47, Tỉnh lộ 38, K5, P5, thành phốBL, Bạc Liêu",
   "Longtitude": "9.2061818",
   "Latitude": "105.7348179"
 },
 {
   "STT": "126",
   "Name": "Nhà thuốc  Tuyết Thảo 1",
   "address": "Số 464 Võ Thị Sáu, P.3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.28599",
   "Latitude": "105.7206622"
 },
 {
   "STT": "127",
   "Name": "Cơ sở bán lẻ thuốc dược liệu, thuốc cổ truyền  Hậu Sanh Đường",
   "address": "Số 63 Lê Văn Duyệt, P. 3, thành phố Bạc Liêu",
   "Longtitude": "9.288302",
   "Latitude": "105.7248716"
 },
 {
   "STT": "128",
   "Name": "Nhà thuốc  Mai Yên",
   "address": "Số 74 Hòa Bình, P.3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.28800429999999",
   "Latitude": "105.7221896"
 },
 {
   "STT": "129",
   "Name": "Quầy thuốc  Ý Niệm",
   "address": "Số 204, ấp Cái Dầy, thị trấn  Châu Hưng, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3432558",
   "Latitude": "105.7149569"
 },
 {
   "STT": "130",
   "Name": "Quầy thuốc  Đăng Khoa",
   "address": "Ấp Thị Trấn A1, thị trấn  Hòa Bình, huyện  Hòa Bình",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "131",
   "Name": "Quầy thuốc  Kiên Nhẫn",
   "address": "Số 524, Khóm 1, P.1, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.260136",
   "Latitude": "105.3753129"
 },
 {
   "STT": "132",
   "Name": "Quầy thuốc  Thiên Phú",
   "address": "Số 100, ấp Vĩnh Hòa, xã Vĩnh Thanh, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.36576509999999",
   "Latitude": "105.488828"
 },
 {
   "STT": "133",
   "Name": "Quầy thuốc   Thanh Liêm",
   "address": "Số 417, ấp Thọ Tiền, xã Phước Long,  Phước Long, Bạc Liêu",
   "Longtitude": "9.4130056",
   "Latitude": "105.3950939"
 },
 {
   "STT": "134",
   "Name": "Quầy thuốc   Thuận Hưng",
   "address": "Số 37, ấp Phước Thành, xã Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.36576509999999",
   "Latitude": "105.488828"
 },
 {
   "STT": "135",
   "Name": "Quầy thuốc  Lý Yến Nhi",
   "address": "Số 418, ấp Phước Thọ Tiền, xã Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4130056",
   "Latitude": "105.3950939"
 },
 {
   "STT": "136",
   "Name": "Quầy thuốc  Trương Tuyết Vân",
   "address": "Số 338B, ấp Nội Ô, thị trấn  Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "137",
   "Name": "Quầy thuốc  Quang Huy",
   "address": "Số 66 ấp cầu Đỏ, xã Vĩnh Lộc, huyện  hồng Dân, Bạc Liêu",
   "Longtitude": "9.5639773",
   "Latitude": "105.3950939"
 },
 {
   "STT": "138",
   "Name": "Cơ sở bán lẻ thuốc dược liệu, thuốc cổ truyền  Tám Tỵ",
   "address": "Số 31 Ấp Ninh Thạnh, xã Ninh Quới A, huyện Hồng Dân",
   "Longtitude": "9.51553049999999",
   "Latitude": "105.5122694"
 },
 {
   "STT": "139",
   "Name": "Quầy thuốc  Quốc Phong",
   "address": "Số 117, ấp Ninh Thạnh, xã Ninh Quới A, huyện  Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.51553049999999",
   "Latitude": "105.5122694"
 },
 {
   "STT": "140",
   "Name": "Quầy thuốc  Trọng Nhi",
   "address": "Số 7, Ấp 3, thị trấn  Gành Hào, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.0268594",
   "Latitude": "105.4195905"
 },
 {
   "STT": "141",
   "Name": "Quầy thuốc  Cúc Hương",
   "address": "Số 191 Ngọc Điền, ấp 2, thị trấn  Gành Hào, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.0267018",
   "Latitude": "105.4191388"
 },
 {
   "STT": "142",
   "Name": "Quầy thuốc  Hồng Công",
   "address": "Số 47, ấp Diêm Điền, xã Điền Hải, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.10787869999999",
   "Latitude": "105.488828"
 },
 {
   "STT": "143",
   "Name": "Quầy thuốc  Hồng Vân",
   "address": "Số 78, ấp Nhà Lầu 2, xã Ninh Thạnh Lợi A, huyện Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.4376772",
   "Latitude": "105.3248269"
 },
 {
   "STT": "144",
   "Name": "Nhà thuốc  Nhà thuốc Trung tâm Y tế huyện Hồng Dân",
   "address": "Số 01 Trần Hưng Đạo, Ấp Nội Ô, thị trấn  Ngan Dừa, huyện  Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "145",
   "Name": "Quầy thuốc  Thành To",
   "address": "Số 123, ấp Mỹ Tường 1, xã Hưng Phú, Phước Long, Bạc Liêu",
   "Longtitude": "9.4113246",
   "Latitude": "105.5532993"
 },
 {
   "STT": "146",
   "Name": "Quầy thuốc  Công Nguyên",
   "address": "Số 221A, ấp Nội Ô, thị trấn  Phước Long, huyện  Phước Long",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "147",
   "Name": "Quầy thuốc  Ngọc Minh",
   "address": "50 Ấp 12, xã vĩnh Hậu A, huyện Hòa Bình, Bạc Liêu",
   "Longtitude": "9.23301459999999",
   "Latitude": "105.6940454"
 },
 {
   "STT": "148",
   "Name": "Quầy thuốc  Phương Anh",
   "address": "Số 01 Lô B, TTTM, Ấp Thị Trấn A, thị trấn  Hòa Bình, huyện  Hòa Bình",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "149",
   "Name": "Quầy thuốc   Thành Phát",
   "address": "144 ấp Vĩnh Lạc, xã Vĩnh Thịnh, Hòa Bình, Bạc Liêu",
   "Longtitude": "9.1886028",
   "Latitude": "105.6060661"
 },
 {
   "STT": "150",
   "Name": "Quầy thuốc  Ái Trân",
   "address": "Số 19, ấp 21, xã Minh Diệu, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.3390177",
   "Latitude": "105.6060661"
 },
 {
   "STT": "151",
   "Name": "Quầy thuốc  Bích Trân",
   "address": "Ấp Thị Trấn B, thị trấn  Hòa Bình, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "152",
   "Name": "Nhà Thuốc  Cẩm Duyên",
   "address": "Số 323, K. 3, P. Láng Tròn, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2735574",
   "Latitude": "105.5064087"
 },
 {
   "STT": "153",
   "Name": "Quầy thuốc  Minh Quân",
   "address": "Số 218 K. 5, P. Hộ Phòng, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "154",
   "Name": "Quầy thuốc  Phước Thịnh",
   "address": "Số 12, P. Hộ Phòng, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "156",
   "Name": "Quầy thuốc  Việt Thành",
   "address": "Số 40 ấp Tam Hưng, xã Vĩnh Hưng, huyện  Vĩnh Lợi, Bạc Liêu",
   "Longtitude": "9.3871631",
   "Latitude": "105.6119301"
 },
 {
   "STT": "157",
   "Name": "Quầy thuốc  Ngọc Giàu",
   "address": "ấp Trung Hưng, xã Vĩnh Hưng A, huyện Vĩnh Lợi, T. Bạc Liêu",
   "Longtitude": "9.3871631",
   "Latitude": "105.6119301"
 },
 {
   "STT": "158",
   "Name": "Quầy thuốc  Song Duy",
   "address": "Số 104, ấp Cái Dầy, thị trấn  Châu Hưng, huyện  Vĩnh Lợi",
   "Longtitude": "9.3432558",
   "Latitude": "105.7149569"
 },
 {
   "STT": "159",
   "Name": "Quầy thuốc  Thanh Tú",
   "address": "ấp Nước Mặn, xã Hưng Hội, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.336645",
   "Latitude": "105.76416"
 },
 {
   "STT": "160",
   "Name": "Quầy thuốc  Thanh Tú",
   "address": "Số 290, ấp Tam Hưng, xã Vĩnh Hưng, huyện  Vĩnh Lợi, Bạc Liêu",
   "Longtitude": "9.3871631",
   "Latitude": "105.6119301"
 },
 {
   "STT": "161",
   "Name": "Quầy thuốc  Hoàng Siêng",
   "address": "Ấp Lung Chim, xã Định Thành, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1323056",
   "Latitude": "105.2955575"
 },
 {
   "STT": "162",
   "Name": "Nhà thuốc  Minh Khuê",
   "address": "Số 97 Bà Triệu, Phường 3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.28718299999999",
   "Latitude": "105.7232618"
 },
 {
   "STT": "163",
   "Name": "Nhà thuốc  Song nghi 1",
   "address": "Số 133 Trần Huỳnh, P.7, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2939884",
   "Latitude": "105.7211467"
 },
 {
   "STT": "164",
   "Name": "Quầy thuốc  Hữu Đạt",
   "address": " Số 01/766 ấp Vĩnh An, xã Vĩnh Trạch, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.37853099999999",
   "Latitude": "105.3946"
 },
 {
   "STT": "165",
   "Name": "Nhà thuốc  Thanh Tuấn",
   "address": "Số 6/379 đường Tỉnh lộ 38, K. 8, P. 5, thành phố Bạc Liêu",
   "Longtitude": "9.2037994",
   "Latitude": "105.7292396"
 },
 {
   "STT": "166",
   "Name": "Nhà thuốc  Tú Phương",
   "address": "Số 5/126 Cầu Kè, K. 5, P. 2, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2800023",
   "Latitude": "105.7210487"
 },
 {
   "STT": "167",
   "Name": "Nhà thuốc  Hồng Nga",
   "address": "Số 57A Hai Bà Trưng, P.3, thành phố Bạc Liêu",
   "Longtitude": "9.2872006",
   "Latitude": "105.7264673"
 },
 {
   "STT": "168",
   "Name": "Quầy thuốc  Hai Thắng",
   "address": "Số 38, Ấp 4, xã Phong Thạnh Tây B, huyện Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.332002",
   "Latitude": "105.2779983"
 },
 {
   "STT": "169",
   "Name": "Quầy thuốc  Số 28",
   "address": "175A Ấp Nội Ô, thị trấn  Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "170",
   "Name": "Quầy thuốc  Trần Huỳnh",
   "address": "Số 11, ấp 4, Xã Phong Thạnh Tây B, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.332002",
   "Latitude": "105.2779983"
 },
 {
   "STT": "171",
   "Name": "Quầy thuốc  Mỹ Tú",
   "address": "09 A ấp Long Hòa, thị trấn  Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.43951429999999",
   "Latitude": "105.462028"
 },
 {
   "STT": "172",
   "Name": "Quầy thuốc  Diên An",
   "address": "Lô 20, Đường Lê Duẩn, TTTM huyện Hồng Dân",
   "Longtitude": "9.55390289999999",
   "Latitude": "105.45205"
 },
 {
   "STT": "173",
   "Name": "Quầy thuốc  Phú Quý",
   "address": "Ấp Ninh Thạnh Tây, xã Ninh Thạnh Lợi, huyện  Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.48890879999999",
   "Latitude": "105.3537812"
 },
 {
   "STT": "174",
   "Name": "Quầy thuốc  Minh Hiệp",
   "address": "Lô 6C Nguyễn Thị Minh Khai, ấp Nội Ô, thị trấn  Ngan Dừa, huyện  Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "175",
   "Name": "Quầy thuốc  Ngọc Nang",
   "address": " Kios 10BẤp Nội Ô, thị Trấn Ngan Dừa, huyện  Hồng Dân,tỉnh Bạc Liêu",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "176",
   "Name": "Quầy thuốc  Hồng Sơn",
   "address": "Số 85B, ấp Nội Ô, Thị trấn Ngan Dừa, huyện  Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "177",
   "Name": "Quầy thuốc  Sơn Ca",
   "address": "Số 151 ấp Nhàn Dân A, xã Tân Phong, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2213584",
   "Latitude": "105.348246"
 },
 {
   "STT": "178",
   "Name": "Quầy thuốc  Phương Thảo",
   "address": "Số 35 ấp Giồng Bướm A, xã Châu Thới, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3601976",
   "Latitude": "105.6548624"
 },
 {
   "STT": "179",
   "Name": "Quầy thuốc  Thanh Tho",
   "address": "Số 68 ấp Thông Lưu, thị trấn  Châu Hưng, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3388612",
   "Latitude": "105.7292491"
 },
 {
   "STT": "180",
   "Name": "Quầy thuốc  Hải Âu",
   "address": "Ấp 2, thị trấn  Gành Hào, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.0336064",
   "Latitude": "105.4275565"
 },
 {
   "STT": "181",
   "Name": "Quầy thuốc  Hữu Thịnh",
   "address": "Ấp Cái Keo, xã An Phúc, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1",
   "Latitude": "105.3"
 },
 {
   "STT": "182",
   "Name": "Quầy thuốc  Kim Tâm",
   "address": "Số 34 Ấp Xóm Lớn A, Xã Vĩnh Mỹ A, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.23258549999999",
   "Latitude": "105.5826123"
 },
 {
   "STT": "183",
   "Name": "Quầy thuốc  Đan Thanh",
   "address": "Số 212 ấp Thị Trấn B, thị trấn  Hòa Bình, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "184",
   "Name": "Quầy thuốc  Mai huỳnh",
   "address": "Số 21, ấp Thị trấn A, thị trấn  Hòa Bình, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "185",
   "Name": "Nhà thuốc  Ngọc Diệp",
   "address": "Số 35 Cách Mạng, P.1, thành phố Bạc Liêu",
   "Longtitude": "9.2962419",
   "Latitude": "105.7370994"
 },
 {
   "STT": "186",
   "Name": "Quầy thuốc  Hoàng Qui",
   "address": "Số 2/419,  ấp Biển Đông B, xã Vĩnh Trạch Đông, thành phố Bạc Liêu",
   "Longtitude": "9.2661286",
   "Latitude": "105.7938069"
 },
 {
   "STT": "187",
   "Name": "Quầy thuốc   Hồng Thu",
   "address": "Số 262/2, ấp Giồng Nhãn, xã Hiệp Thành, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.24115209999999",
   "Latitude": "105.7669687"
 },
 {
   "STT": "188",
   "Name": "Công ty TNHH Dược Phẩm Gia Nguyễn Bạc Liêu",
   "address": "Số 26/129 đường Cao Văn Lầu, P. 2, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2475085",
   "Latitude": "105.7308286"
 },
 {
   "STT": "189",
   "Name": "Quầy thuốc  Diễm My",
   "address": "Ấp Phước Thọ Tiền, xã Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4130056",
   "Latitude": "105.3950939"
 },
 {
   "STT": "190",
   "Name": "Quầy thuốc  Tuyết Mai",
   "address": "Ấp Phước Thành, xã Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4130056",
   "Latitude": "105.3950939"
 },
 {
   "STT": "191",
   "Name": "Quầy thuốc  Huỳnh Khiêm",
   "address": "Số 38/10 K. 2, P. Hộ Phòng, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2234179",
   "Latitude": "105.4243804"
 },
 {
   "STT": "193",
   "Name": "Nhà thuốc   TTYT Huyện Đông Hải",
   "address": "Ấp 4, thị trấn  Gành Hào, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.0383639",
   "Latitude": "105.4199115"
 },
 {
   "STT": "194",
   "Name": "Quầy thuốc  Chí Hải",
   "address": "Ấp Diêm Điền, xã Điền Hải, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1056847",
   "Latitude": "105.4919293"
 },
 {
   "STT": "195",
   "Name": "Quầy thuốc  Hải Âu 2",
   "address": "Ấp A,thị trấn  Gành Hào, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.0336064",
   "Latitude": "105.4275565"
 },
 {
   "STT": "196",
   "Name": "Quầy thuốc  Số 10",
   "address": "Số 212, ấp 2, thị trấn  Gành Hào, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.0349627",
   "Latitude": "105.4282431"
 },
 {
   "STT": "197",
   "Name": "Quầy thuốc  Đông Hải II",
   "address": "Số 66 Lô G, ấp 3, thị trấn  Gành Hào, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.0349627",
   "Latitude": "105.4282431"
 },
 {
   "STT": "198",
   "Name": "Quầy thuốc  Minh Anh",
   "address": "165 đường Nguyễn Trung Trực, Ấp Láng Giài A, thị trấn  Hòa Bình, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.28590629999999",
   "Latitude": "105.6421952"
 },
 {
   "STT": "199",
   "Name": "Quầy thuốc  Kiều Anh",
   "address": "Số 67, ấp Thị Trấn B, thị trấn  Hòa Bình, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "200",
   "Name": "Quầy thuốc  Trung Thu",
   "address": "Số 80, ấp Vĩnh Mẫu, xã  Vĩnh Hậu, huyện Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.2131068",
   "Latitude": "105.6588485"
 },
 {
   "STT": "201",
   "Name": "Quầy thuốc  Bích Châu",
   "address": "Số 96, ấp Thị trấn A, thị trấn  Hòa Bình, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "202",
   "Name": "Quầy thuốc  Tuyết Giang",
   "address": "Số 204, ấp Phước Thạnh 1, xã Long Thạnh, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3092534",
   "Latitude": "105.6705801"
 },
 {
   "STT": "203",
   "Name": "Nhà thuốc  Huyên Thảo",
   "address": "133A/4 khóm 2, P. 7, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.3036569",
   "Latitude": "105.7197071"
 },
 {
   "STT": "204",
   "Name": "Quầy thuốc  Thanh Lam ",
   "address": "5/158B ấp Giồng Giữa B, xã Vĩnh Trạch Đông, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2661286",
   "Latitude": "105.7938069"
 },
 {
   "STT": "205",
   "Name": "Nhà thuốc  Số 1",
   "address": "Số 244, Đường Bà Triệu, P.3, thành phố Bạc Liêu",
   "Longtitude": "9.2875976",
   "Latitude": "105.724107"
 },
 {
   "STT": "206",
   "Name": "Nhà thuốc  Phúc Lộc",
   "address": "Số 38A đường Bà Triệu, Khóm 1, P. 3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2875976",
   "Latitude": "105.724107"
 },
 {
   "STT": "207",
   "Name": "Nhà thuốc  Kim Thoa",
   "address": "Số 20 lô B, đường Hòa Bình, P. 3, thành phố Bạc Liêu,  tỉnh Bạc Liêu",
   "Longtitude": "9.28897939999999",
   "Latitude": "105.7242546"
 },
 {
   "STT": "208",
   "Name": "Nhà thuốc  Nhân Ái",
   "address": "Số 25 Phan Đình Phùng, P. 3, thành phố Bạc Liêu,  tỉnh Bạc Liêu",
   "Longtitude": "9.2879364",
   "Latitude": "105.7264888"
 },
 {
   "STT": "209",
   "Name": "Công ty Cổ Phần Dược Phẩm Bạc Liêu",
   "address": "Số 99 Hoàng Văn Thụ, P. 3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2865769",
   "Latitude": "105.7258377"
 },
 {
   "STT": "210",
   "Name": "Quầy thuốc  Trần Danh",
   "address": "số 9 Nguyễn Thị Minh Khai, thị trấn  Ngan Dừa, huyện huyện  Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.56493959999999",
   "Latitude": "105.4485959"
 },
 {
   "STT": "211",
   "Name": "Quầy thuốc  Thanh Nhàn",
   "address": "Ấp Ninh Thạnh Tây, Ninh Thạnh Lợi, huyện  Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.48890879999999",
   "Latitude": "105.3537812"
 },
 {
   "STT": "212",
   "Name": "Quầy thuốc  Tiến Phúc",
   "address": "Số 100 Khóm 1, P. Hộ Phòng, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2259938",
   "Latitude": "105.4124112"
 },
 {
   "STT": "213",
   "Name": "Quầy thuốc  Thu Hiền",
   "address": "Số 34, Q.Lộ 1A, K. 5, P. Hộ Phòng,thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2346209",
   "Latitude": "105.4438698"
 },
 {
   "STT": "214",
   "Name": "Quầy thuốc  Gia Bảo",
   "address": "Số 259 khóm 2, P. 1, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.260136",
   "Latitude": "105.3753129"
 },
 {
   "STT": "215",
   "Name": "Quầy thuốc  Vũ Gấm",
   "address": "Số 12 Quốc lộ 1A, Khúc Tréo B, xã Tân Phong, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2114215",
   "Latitude": "105.3680365"
 },
 {
   "STT": "216",
   "Name": "Quầy thuốc  Huỳnh Hùng",
   "address": "Số 68 Quốc lộ 1A, khóm 2, P. Hộ Phòng, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.22895",
   "Latitude": "105.4221064"
 },
 {
   "STT": "217",
   "Name": "Quầy thuốc  Quang Nghiệm",
   "address": "Số 192, ấp Sóc Đồn, xã Hưng Hội, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3372716",
   "Latitude": "105.7644596"
 },
 {
   "STT": "218",
   "Name": "Quầy thuốc  Minh Quang",
   "address": "Số 213 ấp Phước Thạnh 1, xã Long Thạnh, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3092534",
   "Latitude": "105.6705801"
 },
 {
   "STT": "219",
   "Name": "Quầy thuốc  Thủy Ngọc",
   "address": "Số 267 ấp Tràm 1, xã Long Thạnh, huyện  Vĩnh Lợi",
   "Longtitude": "9.3281216",
   "Latitude": "105.6545841"
 },
 {
   "STT": "220",
   "Name": "Quầy thuốc  Ngọc Nhi",
   "address": "Số 36, Hương Lộ 6, ấp Cù Lao, xã Hưng Hội, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3372716",
   "Latitude": "105.7644596"
 },
 {
   "STT": "221",
   "Name": "Quầy thuốc  Trung Hiếu",
   "address": "Ấp Cái Dầy, thị trấn  Châu Hưng, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3432558",
   "Latitude": "105.7149569"
 },
 {
   "STT": "222",
   "Name": "Quầy thuốc  Phong Phú",
   "address": "Số 08 ấp Cây Gừa, xã Vĩnh Hậu A, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.1903952",
   "Latitude": "105.6943881"
 },
 {
   "STT": "223",
   "Name": "Quầy thuốc  Bích Trâm",
   "address": "Số 22 ấp Ninh Thạnh, xã Ninh Quới A, huyện  Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.51553049999999",
   "Latitude": "105.5122694"
 },
 {
   "STT": "224",
   "Name": "Quầy thuốc  Số 36- Tuấn",
   "address": "Số 50 ấp Phước Thành, xã Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4006715",
   "Latitude": "105.4302383"
 },
 {
   "STT": "225",
   "Name": "Quầy thuốc  Út Chiến",
   "address": "Ấp 4, xã Phong Thạnh Tây B, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.33897659999999",
   "Latitude": "105.3234646"
 },
 {
   "STT": "226",
   "Name": "Quầy thuốc  Châu Minh Quân",
   "address": "Số 61, ấp 2B, xã Phong Thạnh Tây A,  huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.3376512",
   "Latitude": "105.3692249"
 },
 {
   "STT": "227",
   "Name": "Nhà thuốc  Ngọc Trân",
   "address": "Số 275, Đường 23 tháng 8, P. 8, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2963195",
   "Latitude": "105.7051607"
 },
 {
   "STT": "228",
   "Name": "Nhà thuốc  Ngọc Xuân",
   "address": "Số 127 đường Võ Thị Sáu, P. 8, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2903474",
   "Latitude": "105.7150501"
 },
 {
   "STT": "229",
   "Name": "Nhà thuốc  Ngọc Phú",
   "address": "Số 264 Cách Mạng, P.7, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2940027",
   "Latitude": "105.7215663"
 },
 {
   "STT": "230",
   "Name": "Nhà thuốc  Ngọc Diễn",
   "address": "Số 126, Đường Cách Mạng, P. 1, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2957663",
   "Latitude": "105.7363325"
 },
 {
   "STT": "231",
   "Name": "Nhà thuốc  Số 06",
   "address": "Số 227 đường Trần Huỳnh, P.1, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2968608",
   "Latitude": "105.7276735"
 },
 {
   "STT": "232",
   "Name": "Nhà Thuốc  Thái Ngọc",
   "address": "Số 02 Đặng Thùy Trâm, P. 3,thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2841224",
   "Latitude": "105.7207899"
 },
 {
   "STT": "233",
   "Name": "Nhà Thuốc  Yến Linh",
   "address": "Số 176 Hòa Bình, P. 3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.28949849999999",
   "Latitude": "105.7242763"
 },
 {
   "STT": "235",
   "Name": "Quầy thuốc  Phú Sĩ",
   "address": "Ấp 2B, xã Phong Thạnh Tây A, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.3376512",
   "Latitude": "105.3692249"
 },
 {
   "STT": "236",
   "Name": "Quầy thuốc  Như Ngọc",
   "address": "Số 292 ấp Phước 2, xã Vĩnh Phú Tây, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.3678476",
   "Latitude": "105.4419546"
 },
 {
   "STT": "237",
   "Name": "Quầy thuốc  Vạn An",
   "address": "Ấp Long Thành, thị trấn  Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4314017",
   "Latitude": "105.4612324"
 },
 {
   "STT": "238",
   "Name": "Quầy thuốc  Hoàng Tuấn",
   "address": "Số 303A ấp Long Thành, thị trấn  Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "239",
   "Name": "Nhà thuốc  Ngọc Diệp",
   "address": "Quốc lộ 1A, ấp Thị Trấn A1, thị trấn  Hòa Bình, huyện Hòa Bình",
   "Longtitude": "9.2704595",
   "Latitude": "105.5897386"
 },
 {
   "STT": "240",
   "Name": "Quầy thuốc  Gia An",
   "address": "Số 245, ấp Láng Giài, thị trấn  Hòa Bình, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.28590629999999",
   "Latitude": "105.6421952"
 },
 {
   "STT": "241",
   "Name": "Quầy thuốc  Vĩnh Bình",
   "address": "Số 112 Ấp 18, xã Vĩnh Bình, huyện  Hòa Bình",
   "Longtitude": "9.3444221",
   "Latitude": "105.5608265"
 },
 {
   "STT": "242",
   "Name": "Quầy thuốc  Ngọc Duyên",
   "address": "Số 360 ấp Tân Tạo, thị trấn  Châu Hưng, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3388612",
   "Latitude": "105.7292491"
 },
 {
   "STT": "243",
   "Name": "Quầy thuốc  Giang Văn Bùi",
   "address": "Số 19 ấp Tam Hưng, xã Vĩnh Hưng, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3871631",
   "Latitude": "105.6119301"
 },
 {
   "STT": "244",
   "Name": "Nhà thuốc  Chí Tường",
   "address": "Số 323 đường 23 tháng 8, K. 3, P. 8, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.29653229999999",
   "Latitude": "105.7036138"
 },
 {
   "STT": "245",
   "Name": "Quầy thuốc  Thiên Thanh",
   "address": "Nhà không số, ấp Kim Cấu, xã Vĩnh Trạch, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.3090846",
   "Latitude": "105.7938069"
 },
 {
   "STT": "246",
   "Name": "Quầy thuốc  Triệu Quốc Thái",
   "address": "Số 161A, ấp Nội Ô, thị trấn  Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4529673",
   "Latitude": "105.4653897"
 },
 {
   "STT": "247",
   "Name": "Quầy thuốc  Nhật Linh",
   "address": "Nhà không số, ấp Thào Lạng, xã Vĩnh Trạch, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.3090846",
   "Latitude": "105.7938069"
 },
 {
   "STT": "248",
   "Name": "Quầy thuốc  Thanh Tâm",
   "address": "Ấp 16 xã Vĩnh Hậu A,huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.26035319999999",
   "Latitude": "105.7521537"
 },
 {
   "STT": "249",
   "Name": "Quầy thuốc  Mỹ Thu",
   "address": "Ấp Vĩnh Lạc, xã Vĩnh Thịnh, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.1886028",
   "Latitude": "105.6060661"
 },
 {
   "STT": "250",
   "Name": "Quầy thuốc  Láng Giài",
   "address": "Số 169 Ấp Thị Trấn A, thị trấn Hòa Bình, huyện Hòa Bình, Bạc Liêu",
   "Longtitude": "9.2734758",
   "Latitude": "105.629523"
 },
 {
   "STT": "251",
   "Name": "Quầy thuốc  Gia Phước ",
   "address": "Số 256 Quốc lộ 1A, ấp 15, xã Vĩnh Mỹ B, huyện Hòa Bình, Bạc Liêu",
   "Longtitude": "9.2844527",
   "Latitude": "105.6353704"
 },
 {
   "STT": "252",
   "Name": "Quầy thuốc  Thiên Đức",
   "address": "Ấp Chùa Phật, thị trấn  Hòa Bình, huyện Hòa Bình,tỉnh Bạc Liêu",
   "Longtitude": "9.2739657",
   "Latitude": "105.6324959"
 },
 {
   "STT": "253",
   "Name": "Quầy thuốc  Song Ngọc",
   "address": "Số 4B K. 2, P. Hộ Phòng, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2259938",
   "Latitude": "105.4124112"
 },
 {
   "STT": "254",
   "Name": "Quầy thuốc   Thịnh Linh",
   "address": "Số 298, Khóm 2, P. Láng Tròn, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.3036569",
   "Latitude": "105.7197071"
 },
 {
   "STT": "255",
   "Name": "Quầy thuốc   Gia Hân",
   "address": "Số 172, ấp Khúc Tréo A, xã Tân Phong,thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.1963166",
   "Latitude": "105.3298112"
 },
 {
   "STT": "256",
   "Name": "Quầy thuốc   Lê Kim Danh",
   "address": "Số 168, ấp Khúc Tréo A, xã Tân Phong,thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.1963166",
   "Latitude": "105.3298112"
 },
 {
   "STT": "257",
   "Name": "Quầy thuốc  Kim Huệ",
   "address": "Số 88, ấp Nhàn Dân A, xã Tân Phong, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2213584",
   "Latitude": "105.348246"
 },
 {
   "STT": "258",
   "Name": "Quầy thuốc  Minh Tơ",
   "address": "Số 58/B Ấp 3, xã Phong Thạnh Đông A, thị xã  Giá Rai,  tỉnh Bạc Liêu",
   "Longtitude": "9.2735574",
   "Latitude": "105.5064087"
 },
 {
   "STT": "259",
   "Name": "Quầy thuốc  Bảo Toàn",
   "address": "Số 06, ấp 19, xã Phong Thạnh, thị xã  Giá Rai,  tỉnh Bạc Liêu",
   "Longtitude": "9.3116383",
   "Latitude": "105.393734"
 },
 {
   "STT": "260",
   "Name": "Quầy thuốc  Ngọc Nhiên",
   "address": "Khóm 1, P. 1,  thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.238733",
   "Latitude": "105.4592979"
 },
 {
   "STT": "261",
   "Name": "Quầy thuốc  Thiện Tài",
   "address": "Ấp Bửu II, xã Long Điền Đông, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1487573",
   "Latitude": "105.5357139"
 },
 {
   "STT": "262",
   "Name": "Quầy thuốc  Lâm Văn Năm",
   "address": "Ấp Diêm Điền, xã Điền Hải, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1056847",
   "Latitude": "105.4919293"
 },
 {
   "STT": "263",
   "Name": "Quầy thuốc  Hoàng Huy",
   "address": "Ấp Cây Giang, xã Long Điền, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1676975",
   "Latitude": "105.4481035"
 },
 {
   "STT": "264",
   "Name": "Quầy thuốc  Thảo Quyên",
   "address": "Ấp Bờ Cảng, xã Điền Hải, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.108406",
   "Latitude": "105.489159"
 },
 {
   "STT": "265",
   "Name": "Quầy thuốc  Vạn Thọ",
   "address": "Ấp Nội Ô, thị trấn  Ngan Dừa, huyện  Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.5713437",
   "Latitude": "105.4495395"
 },
 {
   "STT": "266",
   "Name": "Quầy thuốc  Khánh Ngân",
   "address": "Số 122 Quốc lộ 1A, thị trấn  Châu Hưng, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.346774",
   "Latitude": "105.7144571"
 },
 {
   "STT": "267",
   "Name": "Quầy thuốc  Phương Huy",
   "address": "số 40A ấp Xẻo Chích, TT Châu Hưng, huyện Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.34961079999999",
   "Latitude": "105.7292491"
 },
 {
   "STT": "268",
   "Name": "Nhà thuốc  Bích Anh",
   "address": "Số 3/44 đường Tỉnh lộ 38, P. 5, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2037994",
   "Latitude": "105.7292396"
 },
 {
   "STT": "269",
   "Name": "Nhà thuốc  Mai Thu",
   "address": "Số 604/7 Bạch Đằng, P. Nhà Mát, thành phố Bạc Liêu,  tỉnh Bạc Liêu",
   "Longtitude": "9.2269115",
   "Latitude": "105.7362681"
 },
 {
   "STT": "270",
   "Name": "Nhà thuốc  Kiên Cường",
   "address": "Số 178/1 đường Trần Huỳnh, P. 7, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2940027",
   "Latitude": "105.7215663"
 },
 {
   "STT": "271",
   "Name": "Cơ sở bán buôn  Hiền Mai",
   "address": "Số 10-12 Hai Bà Trưng, P. 3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.28666009999999",
   "Latitude": "105.7249784"
 },
 {
   "STT": "273",
   "Name": "Quầy thuốc  Kiều Phí",
   "address": "Ấp Lung Lá, xã Định Thành A, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1225686",
   "Latitude": "105.2721456"
 },
 {
   "STT": "274",
   "Name": "Quầy thuốc  Thanh Nhã",
   "address": "Số 2, ấp Cây Thẻ, xã Định Thành, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1328988",
   "Latitude": "105.2962012"
 },
 {
   "STT": "275",
   "Name": "Quầy thuốc  Phương Đoan",
   "address": "Ấp Văn Đức A, xã An Trạch, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1763456",
   "Latitude": "105.3950939"
 },
 {
   "STT": "276",
   "Name": "Quầy thuốc  Thịnh Cường",
   "address": "Ấp Cây Thẻ, xã Định Thành, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.2735874",
   "Latitude": "105.3833808"
 },
 {
   "STT": "277",
   "Name": "Quầy thuốc  Ngọc Điệp",
   "address": "Số 290 ấp Hòa Thạnh, xã Long Điền, huyện  Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1676975",
   "Latitude": "105.4481035"
 },
 {
   "STT": "278",
   "Name": "Quầy thuốc  Bích Thảo",
   "address": "Số A01 Ấp 3, TTTM, thị trấn  Gành Hào, huyện  Đông Hải, Bạc Liêu",
   "Longtitude": "9.0349627",
   "Latitude": "105.4282431"
 },
 {
   "STT": "279",
   "Name": "Quầy thuốc  Lộc",
   "address": "Số 51, Hương Lộ 6,  ấp Cù Lao, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3450296",
   "Latitude": "105.7116464"
 },
 {
   "STT": "280",
   "Name": "Quầy thuốc  An Khang",
   "address": "Ấp Năm Căn, xã Hưng Thành, huyện Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.3714248",
   "Latitude": "105.8407722"
 },
 {
   "STT": "281",
   "Name": "Quầy thuốc  Hồng Giang",
   "address": "Tỉnh lộ 38, ấp Kim Cấu, xã Vĩnh Trạch, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2037994",
   "Latitude": "105.7292396"
 },
 {
   "STT": "282",
   "Name": "Quầy thuốc    Kim Hưng",
   "address": "Số 7/238, ấp Biển Tây A, xã Vĩnh Trạch Đông, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2661286",
   "Latitude": "105.7938069"
 },
 {
   "STT": "283",
   "Name": "Nhà thuốc  Số 4",
   "address": "Số 250/22B Cao Văn Lầu, K. 3, P.5, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2836754",
   "Latitude": "105.7252548"
 },
 {
   "STT": "284",
   "Name": "Nhà thuốc  Bệnh Xá Công An",
   "address": "Số 74 Lê Duẩn, K, 7, P. 1, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.3004123",
   "Latitude": "105.7303371"
 },
 {
   "STT": "285",
   "Name": "Nhà thuốc  Tuyết Thảo ",
   "address": "Số 370 Võ Thị Sáu, P.3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2877425",
   "Latitude": "105.7186243"
 },
 {
   "STT": "286",
   "Name": "Quầy thuốc  Kim Phí",
   "address": "Ấp Xóm Mới, xã Tân Thạnh, thị xã  Giá Rai",
   "Longtitude": "9.17605599999999",
   "Latitude": "105.2794311"
 },
 {
   "STT": "287",
   "Name": "Quầy thuốc  Chúc Dư",
   "address": "Ấp Nhàn Dân B, xã Tân Phong, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.207849",
   "Latitude": "105.356286"
 },
 {
   "STT": "288",
   "Name": "Quầy thuốc  Hồng Nghi",
   "address": "Ấp 10B, xã Tân Phong, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.1989667",
   "Latitude": "105.3372637"
 },
 {
   "STT": "289",
   "Name": "Quầy thuốc  Ngọc Mai",
   "address": "Quốc lộ 1A, ấp Phước Thạnh 1, xã Long Thạnh, huyện  Vĩnh Lợi, tỉnh Bạc Liêu",
   "Longtitude": "9.293877",
   "Latitude": "105.6744794"
 },
 {
   "STT": "290",
   "Name": "Quầy thuốc   Bình Nguyên",
   "address": "Số 278B, ấp Tân Long, Long Thạnh, huyện Vĩnh Lợi, Bạc Liêu",
   "Longtitude": "9.3092534",
   "Latitude": "105.6705801"
 },
 {
   "STT": "291",
   "Name": "Quầy thuốc  Tùng Bách",
   "address": "Lô 10 Khu Phố A, Chợ Vĩnh Hưng, ấp Tam Hưng, xã Vĩnh Hưng, huyện  Vĩnh Lợi, Bạc Liêu",
   "Longtitude": "9.389854",
   "Latitude": "105.5994218"
 },
 {
   "STT": "293",
   "Name": "Quầy thuốc  Nam Việt",
   "address": "Ấp Cái Dầy, thị trấn  Châu Hưng, huyện  Vĩnh Lợi, Bạc Liêu",
   "Longtitude": "9.3432558",
   "Latitude": "105.7149569"
 },
 {
   "STT": "294",
   "Name": "Quầy thuốc  Phúc Khang",
   "address": "Phía trước bên phải số 259, ấp Cái Dầy,thị trấn  Châu Hưng, huyện  Vĩnh Lợi, Bạc Liêu",
   "Longtitude": "9.3432558",
   "Latitude": "105.7149569"
 },
 {
   "STT": "295",
   "Name": "Quầy thuốc  Ngô Phát",
   "address": "Quốc lộ 1A, ấp 15, xã Vĩnh Mỹ B, huyện  Hòa Bình",
   "Longtitude": "9.2798576",
   "Latitude": "105.5597305"
 },
 {
   "STT": "296",
   "Name": "Quầy thuốc  Hồng Minh",
   "address": "Số 18/1 QL1A- ấp An Khoa-Vĩnh Mỹ B- Hòa Bình",
   "Longtitude": "9.272243",
   "Latitude": "105.628129"
 },
 {
   "STT": "297",
   "Name": "Quầy thuốc  Vũ Khôi",
   "address": "Ấp Chùa Phật, thị trấn  Hòa Bình, huyện  Hòa Bình, tỉnh Bạc Liêu",
   "Longtitude": "9.2739657",
   "Latitude": "105.6324959"
 },
 {
   "STT": "299",
   "Name": "Quầy thuốc  Hương Thắm",
   "address": "Số 35, ấp Phước Thành, xã Phước Long, huyện Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4006715",
   "Latitude": "105.4302383"
 },
 {
   "STT": "300",
   "Name": "Quầy thuốc  Ngọc Trân",
   "address": "Số 262B ấp Long Thành, thị trấn  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.3092534",
   "Latitude": "105.6705801"
 },
 {
   "STT": "301",
   "Name": "Bán lẻ thuốc dược liệu và thuốc cổ truyền  Vinh Hưng",
   "address": "123 đường Hòa Bình, Phường 7, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.28851349999999",
   "Latitude": "105.722841"
 },
 {
   "STT": "302",
   "Name": "Quầy thuốc  Thanh Tùng",
   "address": "Lô 28 ấp Phú Tân, xã Ninh Quới, huyện Hồng Dân, tỉnh Bạc Liêu",
   "Longtitude": "9.55760499999999",
   "Latitude": "105.5357139"
 },
 {
   "STT": "303",
   "Name": "Quầy thuốc  Đặng Thúy",
   "address": "Ấp Long Thành, thị trấn  Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4314017",
   "Latitude": "105.4612324"
 },
 {
   "STT": "304",
   "Name": "Quầy thuốc  Vũ Luân",
   "address": "Đường Nguyễn Thị Mười, xã Hưng Phú, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4113246",
   "Latitude": "105.5532993"
 },
 {
   "STT": "305",
   "Name": "Quầy thuốc  Thảo Nguyên",
   "address": "Ấp Long Thành, thị trấn  Phước Long, huyện  Phước Long, tỉnh Bạc Liêu",
   "Longtitude": "9.4314017",
   "Latitude": "105.4612324"
 },
 {
   "STT": "306",
   "Name": "Quầy thuốc  Duy Tân",
   "address": "Ấp 13, xã Phong Thạnh Đông, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.3169",
   "Latitude": "105.4883"
 },
 {
   "STT": "307",
   "Name": "Quầy thuốc  Kim Lý",
   "address": "Số 168, Ấp 1, xã Tân Phong, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.2213584",
   "Latitude": "105.348246"
 },
 {
   "STT": "308",
   "Name": "Quầy thuốc  Khải Hoàng",
   "address": "Số 413, Khóm 1, P. 1, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.260136",
   "Latitude": "105.3753129"
 },
 {
   "STT": "309",
   "Name": "Quầy thuốc  Long Thịnh",
   "address": "Số 44 Khóm 1, P. Láng Tròn, thị xã  Giá Rai, tỉnh Bạc Liêu",
   "Longtitude": "9.251623",
   "Latitude": "105.513886"
 },
 {
   "STT": "310",
   "Name": "Nhà thuốc   Thanh Vũ Medicbaclieu",
   "address": "Số 02DN, Đường Tránh QL1A, K. 1, P. 7, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.3112002",
   "Latitude": "105.7159208"
 },
 {
   "STT": "311",
   "Name": "Chi nhánh Công ty cổ phần Dược Hậu Giang tại Bạc Liêu",
   "address": "số 67, Nguyễn Thị Định, khóm 10, phường 1, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.3076594",
   "Latitude": "105.7328168"
 },
 {
   "STT": "312",
   "Name": "Nhà thuốc  Ngọc Nhi",
   "address": "157, Nguyễn Thị Minh Khai, khóm 2, phường 5, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2833256",
   "Latitude": "105.7278596"
 },
 {
   "STT": "313",
   "Name": "Quầy thuốc  Thành Đại",
   "address": "ấp Bửu 2, xã Long Điền Đông, huyện Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1487573",
   "Latitude": "105.5357139"
 },
 {
   "STT": "315",
   "Name": "Quầy thuốc  Thiện Phước",
   "address": "số 46, ấp Diêm Điền, xã Điền Hải, huyện Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.10787869999999",
   "Latitude": "105.488828"
 },
 {
   "STT": "316",
   "Name": "Quầy thuốc  Ngọc Hân",
   "address": "số 39, ấp Thạnh II, xã long Điền, huyện Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1676975",
   "Latitude": "105.4481035"
 },
 {
   "STT": "317",
   "Name": "Quầy thuốc  Mỹ Ảnh",
   "address": "ấp Ba mến, xã An Trạch A, huyện Đông Hải, tỉnh Bạc Liêu",
   "Longtitude": "9.1751556",
   "Latitude": "105.3773249"
 },
 {
   "STT": "318",
   "Name": "Cơ sở bán buôn thuốc thành phẩm  Thái Nhựt",
   "address": "Số 07 Lầu 1, đường Tôn Đức Thắng, P. 1, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2984779",
   "Latitude": "105.7356342"
 },
 {
   "STT": "319",
   "Name": "Công ty TNHH Dược Phẩm Gia Nguyễn Bạc Liêu",
   "address": "Số 26/129 đường Cao Văn Lầu, P. 2, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2475085",
   "Latitude": "105.7308286"
 },
 {
   "STT": "320",
   "Name": "Công ty Cổ Phần Dược Phẩm Bạc Liêu",
   "address": "Số 99 Hoàng Văn Thụ, P. 3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.2865769",
   "Latitude": "105.7258377"
 },
 {
   "STT": "321",
   "Name": "Hiền Mai",
   "address": "Số 10-12 Hai Bà Trưng, P. 3, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.28666009999999",
   "Latitude": "105.7249784"
 },
 {
   "STT": "322",
   "Name": "Chi nhánh Công ty cổ phần Dược Hậu Giang tại Bạc Liêu",
   "address": "số 67, Nguyễn Thị Định, khóm 10, phường 1, thành phố Bạc Liêu, tỉnh Bạc Liêu",
   "Longtitude": "9.3076594",
   "Latitude": "105.7328168"
 }
];